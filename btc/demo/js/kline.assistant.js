function Kline() {}
Kline.prototype = {
    browerState: 0,
    klineWebsocket: null ,
    klineTradeInit: false,
    tradeDate: new Date(),
    tradesLimit: 100,
    lastDepth: null ,
    depthShowSize: 15,
    priceDecimalDigits: 6,
    amountDecimalDigits: 4,
    symbol: null ,
    curPrice: null ,
    title: "",
    reset: function(a) {
        this.refreshUrl(a);
        $("#markettop li a").removeClass("selected");
        $("#markettop li." + a + " a").addClass("selected");
        this.symbol = a;
        this.lastDepth = null ;
        this.curPrice = null ;
        this.klineTradeInit = false;
        $("#trades .trades_list").empty();
        $("#gasks .table").empty();
        $("#gbids .table").empty();
        $("#asks .table").empty();
        $("#bids .table").empty();
        this.websocketRedister(a)
    },
    setTitle: function() {
        document.title = (this.curPrice == null  ? "" : this.curPrice + " ") + this.title;
        // setMate(this.title)
    },
    dateFormatTf: function(a) {
        return (a < 10 ? "0" : "") + a
    },
    dateFormat: function(a) {
        return a.getFullYear() + "-" + this.dateFormatTf(a.getMonth() + 1) + "-" + this.dateFormatTf(a.getDate()) + " " + this.dateFormatTf(a.getHours()) + ":" + this.dateFormatTf(a.getMinutes()) + ":" + this.dateFormatTf(a.getSeconds())
    },
    dateInit: function(b) {
        var a = new Date();
        if (b) {
            a.setTime(b)
        }
        $(".m_rightbot").text(this.dateFormat(a));
        var c = this;
        setInterval(function() {
            a.setTime(a.getTime() + 1000);
            $(".m_rightbot").text(c.dateFormat(a))
        }, 1000)
    },
    //第一次请求的数据
    websocketRedister: function(symbol) {
        var $this = this;
		//$.get('/market/depths?depth=' + symbol + '&v=' + Math.random(), function(result) {
			result={
        		"asks":[[15000.00000000, 0.0700],
        		[10000.00000000, 0.3427],
        		[9999.00000000, 3.3247],
        		[9975.00000000, 2.1080],
        		[8000.00000000, 0.0500],
        		[7913.00000000, 1.9980],
        		[7588.00000000, 3.0000],
        		[6000.00000000, 0.2633],
        		[5990.01000000, 1.0958],
        		[5888.00000000, 1.0000],
        		[5800.00000000, 0.5464],
        		[5700.00000000, 0.0100],
        		[5650.00000000, 0.1000],
        		[5600.00000000, 0.1000],
        		[5550.00000000, 0.0299],
        		[5500.00000000, 2.2443],
        		[5450.00000000, 0.1000],
        		[5400.00000000, 0.6753],
        		[5389.00000000, 0.1398],
        		[5350.00000000, 3.9960],
        		[5300.00000000, 3.2940],
        		[5250.00000000, 1.0000],
        		[5200.00000000, 15.8176],
        		[5180.58000000, 0.0249],
        		[5160.00000000, 0.1998],
        		[5150.00000000, 1.3397],
        		[5100.00000000, 1.1819],
        		[5090.00000000, 1.0000],
        		[5085.00000000, 0.3696],
        		[5050.00000000, 1.6334],
        		[5006.00000000, 0.0169],
        		[5000.00000000, 8.1005],
        		[4990.00000000, 1.0000],
        		[4960.22000000, 0.0120],
        		[4900.00000000, 3.5302],
        		[4850.00000000, 0.2073],
        		[4809.99000000, 0.0200],
        		[4800.00000000, 6.8124],
        		[4780.00000000, 0.9990],
        		[4700.00000000, 1.0000],
        		[4688.00000000, 1.0000],
        		[4600.00000000, 1.6796],
        		[4583.00000000, 0.1820],
        		[4580.00000000, 12.6992],
        		[4550.03000000, 1.0789],
        		[4550.00000000, 0.3475],
        		[4548.00000000, 0.7000],
        		[4544.00000000, 0.6370],
        		[4537.00000000, 0.0199],
        		[4530.00000000, 8.4549],
        		[4529.00000000, 0.2000],
        		[4512.00000000, 0.1997],
        		[4501.00000000, 0.0239],
        		[4500.00000000, 10.4877],
        		[4498.63000000, 1.9980],
        		[4478.00000000, 0.7000],
        		[4474.00000000, 0.1500],
        		[4473.00000000, 0.0100],
        		[4455.00000000, 0.0229],
        		[4450.00000000, 1.0670],
        		[4431.00000000, 1.0000],
        		[4400.00000000, 7.9454],
        		[4380.00000000, 6.1489],
        		[4370.00000000, 0.1630],
        		[4350.00000000, 8.6533],
        		[4348.88000000, 1.6983],
        		[4346.00000000, 0.0475],
        		[4344.00000000, 0.1027],
        		[4330.00000000, 0.2959],
        		[4300.00000000, 1.9791],
        		[4295.00000000, 2.0000],
        		[4292.00000000, 3.0939],
        		[4288.00000000, 2.0267],
        		[4287.99000000, 1.9700],
        		[4282.01000000, 1.3922],
        		[4282.00000000, 1.7900],
        		[4280.00000000, 0.4595],
        		[4279.00000000, 3.0000],
        		[4278.99000000, 2.0650],
        		[4272.00000000, 0.5150],
        		[4270.00000000, 1.0000],
        		[4265.00000000, 1.6074],
        		[4264.00000000, 0.9990],
        		[4259.00000000, 0.5412]],
        		"bids":[[4240.01000000, 1.7447],
        		[4240.00000000, 1.7229],
        		[4238.00000000, 1.1751],
        		[4237.00000000, 0.1801],
        		[4236.00000000, 0.5100],
        		[4224.00000000, 0.5000],
        		[4223.01000000, 0.5100],
        		[4180.02000000, 2.0000],
        		[4180.01000000, 2.0000],
        		[4180.00000000, 0.4000],
        		[4178.00000000, 0.0758],
        		[4170.00000000, 0.3300],
        		[4168.00000000, 0.4030],
        		[4150.00000000, 0.2800],
        		[4140.00000000, 0.1703],
        		[4101.00000000, 1.0000],
        		[4100.00000000, 5.2000],
        		[4058.00000000, 0.1100],
        		[4030.00000000, 2.0000],
        		[4026.00000000, 0.1100],
        		[4018.00000000, 4.0000],
        		[4001.00000000, 1.0000],
        		[4000.00000000, 0.1000],
        		[3900.00000000, 1.0000],
        		[3871.00000000, 0.5000],
        		[3821.00000000, 3.0400],
        		[3818.60000000, 0.2000],
        		[3800.00000000, 2.8300],
        		[3602.00000000, 0.1000],
        		[3600.00000000, 2.0000],
        		[3500.00000000, 1.1000],
        		[3456.00000000, 2.4600],
        		[3400.00000000, 2.0000],
        		[3200.00000000, 3.0000],
        		[3180.00000000, 5.0000],
        		[3001.80000000, 0.2800],
        		[3000.00000000, 1.4100],
        		[2987.03000000, 0.2500],
        		[2800.00000000, 0.1000],
        		[2718.00000000, 1.2000],
        		[2704.50000000, 0.5000],
        		[2700.00000000, 0.0100],
        		[2670.00000000, 0.0100],
        		[2650.00000000, 0.1200],
        		[2630.00000000, 2.6000],
        		[2578.00000000, 10.0000],
        		[2575.00000000, 100.0000],
        		[2439.00000000, 1.1922],
        		[2000.00000000, 0.0100],
        		[10.00000000, 200.0000],
        		[1.00000000, 200.0000]],
        		"date":1467254341
        	}
		    $this.updateDepth(result);
		//});

        setInterval(function() {
            //$.get('/market/depths?depth=' + symbol + '&v=' + Math.random(), function(result) {
            	result={
        		"asks":[[15000.00000000, 0.0700],
        		[10000.00000000, 0.3427],
        		[9999.00000000, 3.3247],
        		[9975.00000000, 2.1080],
        		[8000.00000000, 0.0500],
        		[7913.00000000, 1.9980],
        		[7588.00000000, 3.0000],
        		[6000.00000000, 0.2633],
        		[5990.01000000, 1.0958],
        		[5888.00000000, 1.0000],
        		[5800.00000000, 0.5464],
        		[5700.00000000, 0.0100],
        		[5650.00000000, 0.1000],
        		[5600.00000000, 0.1000],
        		[5550.00000000, 0.0299],
        		[5500.00000000, 2.2443],
        		[5450.00000000, 0.1000],
        		[5400.00000000, 0.6753],
        		[5389.00000000, 0.1398],
        		[5350.00000000, 3.9960],
        		[5300.00000000, 3.2940],
        		[5250.00000000, 1.0000],
        		[5200.00000000, 15.8176],
        		[5180.58000000, 0.0249],
        		[5160.00000000, 0.1998],
        		[5150.00000000, 1.3397],
        		[5100.00000000, 1.1819],
        		[5090.00000000, 1.0000],
        		[5085.00000000, 0.3696],
        		[5050.00000000, 1.6334],
        		[5006.00000000, 0.0169],
        		[5000.00000000, 8.1005],
        		[4990.00000000, 1.0000],
        		[4960.22000000, 0.0120],
        		[4900.00000000, 3.5302],
        		[4850.00000000, 0.2073],
        		[4809.99000000, 0.0200],
        		[4800.00000000, 6.8124],
        		[4780.00000000, 0.9990],
        		[4700.00000000, 1.0000],
        		[4688.00000000, 1.0000],
        		[4600.00000000, 1.6796],
        		[4583.00000000, 0.1820],
        		[4580.00000000, 12.6992],
        		[4550.03000000, 1.0789],
        		[4550.00000000, 0.3475],
        		[4548.00000000, 0.7000],
        		[4544.00000000, 0.6370],
        		[4537.00000000, 0.0199],
        		[4530.00000000, 8.4549],
        		[4529.00000000, 0.2000],
        		[4512.00000000, 0.1997],
        		[4501.00000000, 0.0239],
        		[4500.00000000, 10.4877],
        		[4498.63000000, 1.9980],
        		[4478.00000000, 0.7000],
        		[4474.00000000, 0.1500],
        		[4473.00000000, 0.0100],
        		[4455.00000000, 0.0229],
        		[4450.00000000, 1.0670],
        		[4431.00000000, 1.0000],
        		[4400.00000000, 7.9454],
        		[4380.00000000, 6.1489],
        		[4370.00000000, 0.1630],
        		[4350.00000000, 8.6533],
        		[4348.88000000, 1.6983],
        		[4346.00000000, 0.0475],
        		[4344.00000000, 0.1027],
        		[4330.00000000, 0.2959],
        		[4300.00000000, 1.9791],
        		[4295.00000000, 2.0000],
        		[4292.00000000, 3.0939],
        		[4288.00000000, 2.0267],
        		[4287.99000000, 1.9700],
        		[4282.01000000, 1.3922],
        		[4282.00000000, 1.7900],
        		[4280.00000000, 0.4595],
        		[4279.00000000, 3.0000],
        		[4278.99000000, 2.0650],
        		[4272.00000000, 0.5150],
        		[4270.00000000, 1.0000],
        		[4265.00000000, 1.6074],
        		[4264.00000000, 0.9990],
        		[4259.00000000, 0.5412]],
        		"bids":[[4240.01000000, 1.7447],
        		[4240.00000000, 1.7229],
        		[4238.00000000, 1.1751],
        		[4237.00000000, 0.1801],
        		[4236.00000000, 0.5100],
        		[4224.00000000, 0.5000],
        		[4223.01000000, 0.5100],
        		[4180.02000000, 2.0000],
        		[4180.01000000, 2.0000],
        		[4180.00000000, 0.4000],
        		[4178.00000000, 0.0758],
        		[4170.00000000, 0.3300],
        		[4168.00000000, 0.4030],
        		[4150.00000000, 0.2800],
        		[4140.00000000, 0.1703],
        		[4101.00000000, 1.0000],
        		[4100.00000000, 5.2000],
        		[4058.00000000, 0.1100],
        		[4030.00000000, 2.0000],
        		[4026.00000000, 0.1100],
        		[4018.00000000, 4.0000],
        		[4001.00000000, 1.0000],
        		[4000.00000000, 0.1000],
        		[3900.00000000, 1.0000],
        		[3871.00000000, 0.5000],
        		[3821.00000000, 3.0400],
        		[3818.60000000, 0.2000],
        		[3800.00000000, 2.8300],
        		[3602.00000000, 0.1000],
        		[3600.00000000, 2.0000],
        		[3500.00000000, 1.1000],
        		[3456.00000000, 2.4600],
        		[3400.00000000, 2.0000],
        		[3200.00000000, 3.0000],
        		[3180.00000000, 5.0000],
        		[3001.80000000, 0.2800],
        		[3000.00000000, 1.4100],
        		[2987.03000000, 0.2500],
        		[2800.00000000, 0.1000],
        		[2718.00000000, 1.2000],
        		[2704.50000000, 0.5000],
        		[2700.00000000, 0.0100],
        		[2670.00000000, 0.0100],
        		[2650.00000000, 0.1200],
        		[2630.00000000, 2.6000],
        		[2578.00000000, 10.0000],
        		[2575.00000000, 100.0000],
        		[2439.00000000, 1.1922],
        		[2000.00000000, 0.0100],
        		[10.00000000, 200.0000],
        		[1.00000000, 200.0000]],
        		"date":1467254341
        	}
                $this.updateDepth(result);
            //});
        }, 10000);
		return false;
    },
    pushTrades: function(k) {
        var g = $("#trades .trades_list");
        var a = "";
        for (var d = 0; d < k.length; d++) {
            var l = k[d];
            if (d >= k.length - this.tradesLimit) {
                this.tradeDate.setTime(l.time * 1000);
                var b = this.dateFormatTf(this.tradeDate.getHours()) + ":" + this.dateFormatTf(this.tradeDate.getMinutes()) + ":" + this.dateFormatTf(this.tradeDate.getSeconds());
                var e = (l.amount + "").split(".");
                if (this.klineTradeInit) {
                    a = "<ul class='newul'><li class='tm'>" + b + "</li><li class='pr-" + (l.type == "buy" ? "green" : "red") + "'>" + l.price + "</li><li class='vl'>" + e[0] + "<g>" + (e.length > 1 ? "." + e[1] : "") + "</g></li></ul>" + a
                } else {
                    a = "<ul><li class='tm'>" + b + "</li><li class='pr-" + (l.type == "buy" ? "green" : "red") + "'>" + l.price + "</li><li class='vl'>" + e[0] + "<g>" + (e.length > 1 ? "." + e[1] : "") + "</g></li></ul>" + a
                }
            }
        }
        var c = 0;
        var h = this;
        if (this.klineTradeInit) {
            clearInterval(f);
            var f = setInterval(function() {
                var i = k[c];
                h.curPrice = (i.price).toFixed(2);
                h.setTitle();
                $("div#price").attr("class", i.type == "buy" ? "green" : "red").text(h.curPrice);
                c++;
                if (c >= k.length) {
                    clearInterval(f)
                }
            }, 100)
        } else {
            if (k.length > 0) {
                this.curPrice = k[k.length - 1].price.toFixed(2);
                this.setTitle();
                $("div#price").attr("class", k[k.length - 1].type == "buy" ? "green" : "red").text(this.curPrice)
            }
        }
        if (this.klineTradeInit) {
            g.prepend(a)
        } else {
            g.append(a)
        }
        a = null ;
        g.find("ul.newul").slideDown(1000, function() {
            $(this).removeClass("newul")
        });
        g.find("ul:gt(" + (this.tradesLimit - 1) + ")").remove()
    },
    updateDepth: function(e) {
        window._set_current_depth(e);
        if (!e) {
            return
        }
        $("#gasks .table").html(this.getgview(this.getgasks(e.asks)));
        $("#gbids .table").html(this.getgview(this.getgbids(e.bids)));
        if (this.lastDepth == null ) {
            this.lastDepth = {};
            this.lastDepth.asks = this.getAsks(e.asks, this.depthShowSize);
            this.depthInit(this.lastDepth.asks, $("#asks .table"));
            this.lastDepth.bids = this.getBids(e.bids, this.depthShowSize);
            this.depthInit(this.lastDepth.bids, $("#bids .table"))
        } else {
            var b = $("#asks .table");
            b.find("div.remove").remove();
            b.find("div.add").removeClass("add");
            var f = this.getAsks(e.asks, this.depthShowSize);
            var a = this.lastDepth.asks;
            this.lastDepth.asks = f;
            this.asksAndBids(f.slice(0), a, b);
            var d = $("#bids .table");
            d.find("div.remove").remove();
            d.find("div.add").removeClass("add");
            var g = this.getBids(e.bids, this.depthShowSize);
            var c = this.lastDepth.bids;
            this.lastDepth.bids = g;
            this.asksAndBids(g.slice(0), c, $("#bids .table"))
        }
    },
    depthInit: function(f, h) {
        h.empty();
        if (f && f.length > 0) {
            var g, b = "";
            for (var e = 0; e < f.length; e++) {
                var a = (f[e][0] + "").split(".");
                var d = this.getPrice(a, g);
                g = a[0];
                a = (f[e][1] + "").split(".");
                var c = this.getAmount(a);
                b += "<div class='row'><span class='price'>" + d[0] + "<g>" + d[1] + "</g></span> <span class='amount'>" + c[0] + "<g>" + c[1] + "</g></span></div>"
            }
            h.append(b);
            b = null
        }
    },
    asksAndBids: function(b, c, l) {
        for (var f = 0; f < c.length; f++) {
            var n = false;
            for (var d = 0; d < b.length; d++) {
                if (c[f][0] == b[d][0]) {
                    n = true;
                    if (c[f][1] != b[d][1]) {
                        var a = l.find("div:eq(" + f + ") .amount");
                        a.addClass(c[f][1] > b[d][1] ? "red" : "green");
                        var g = this.getAmount((b[d][1] + "").split("."));
                        setTimeout((function(j, i) {
                            return function() {
                                j.html(i[0] + "<g>" + i[1] + "</g>");
                                j.removeClass("red").removeClass("green");
                                j = null ;
                                i = null
                            }
                        })(a, g), 500)
                    }
                    b.splice(d, 1);
                    break
                }
            }
            if (!n) {
                l.find("div:eq(" + f + ")").addClass("remove");
                c[f][2] = -1
            }
        }
        for (var d = 0; d < c.length; d++) {
            for (var f = 0; f < b.length; f++) {
                if (b[f][0] > c[d][0]) {
                    var k = (b[f][1] + "").split(".");
                    var g = this.getAmount(k);
                    l.find("div:eq(" + d + ")").before("<div class='row add'><span class='price'></span> <span class='amount'>" + g[0] + "<g>" + g[1] + "</g></span></div>");
                    c.splice(d, 0, b[f]);
                    b.splice(f, 1);
                    break
                }
            }
        }
        var h = "";
        for (var f = 0; f < b.length; f++) {
            c.push(b[f]);
            var k = (b[f][1] + "").split(".");
            var g = this.getAmount(k);
            h += "<div class='row add'><span class='price'></span> <span class='amount'>" + g[0] + "<g>" + g[1] + "</g></span></div>"
        }
        if (h.length > 0) {
            l.append(h)
        }
        h = null ;
        var m;
        for (var f = 0; f < c.length; f++) {
            var o = l.find("div:eq(" + f + ")");
            if (!(c[f].length >= 3 && c[f][2] == -1)) {
                var k = (c[f][0] + "").split(".");
                var e = this.getPrice(k, m);
                m = k[0];
                o.find(".price").html(e[0] + "<g>" + e[1] + "</g>")
            }
        }
        b = null ;
        c = null ;
        l.find("div.add").slideDown(800);
        setTimeout((function(i, j) {
            return function() {
                i.slideUp(500, function() {
                    $(this).remove()
                });
                j.removeClass("add")
            }
        })(l.find("div.remove"), l.find("div.add")), 1000)
    },
    getAsks: function(b, a) {
        if (b.length > a) {
            b.splice(0, b.length - a)
        }
        return b
    },
    getBids: function(b, a) {
        if (b.length > a) {
            b.splice(a, b.length - 1)
        }
        return b
    },
    getgview: function(c) {
        var d = "";
        var e;
        for (var b = 0; b < c.length; b++) {
            var a = c[b][0].split(".");
            if (a.length == 1 || a[0] != e) {
                d += "<div class='row'><span class='price'>" + c[b][0] + "</span> <span class='amount'>" + c[b][1] + "</span></div>";
                e = a[0]
            } else {
                d += "<div class='row'><span class='price'><h>" + a[0] + ".</h>" + a[1] + "</span> <span class='amount'>" + c[b][1] + "</span></div>"
            }
        }
        return d
    },
    getgasks: function(j) {
        var k = j[j.length - 1][0];
        var e = j[0][0];
        var a = e - k;
        var d = this.getBlock(a, 100);
        var b = Math.abs(Number(Math.log(d) / Math.log(10))).toFixed(0);
        if (a / d < 2) {
            d = d / 2;
            b++
        }
        if (d >= 1) {
            (b = 0)
        }
        k = parseInt(k / d) * d;
        e = parseInt(e / d) * d;
        var h = [];
        var g = 0;
        for (var f = j.length - 1; f >= 0; f--) {
            if (j[f][0] > k) {
                var c = parseInt(g, 10);
                if (c > 0) {
                    h.unshift([Number(k).toFixed(b), c])
                }
                if (k >= e) {
                    break
                }
                k += d
            }
            g += j[f][1]
        }
        return h
    },
    getgbids: function(j) {
        var k = j[j.length - 1][0];
        var e = j[0][0];
        var a = e - k;
        var d = this.getBlock(a, 100);
        var b = Math.abs(Number(Math.log(d) / Math.log(10))).toFixed(0);
        if (a / d < 2) {
            d = d / 2;
            b++
        }
        if (d >= 1) {
            (b = 0)
        }
        k = parseInt(k / d) * d;
        e = parseInt(e / d) * d;
        var h = [];
        var g = 0;
        for (var f = 0; f < j.length; f++) {
            if (j[f][0] < e) {
                var c = parseInt(g, 10);
                if (c > 0) {
                    h.push([Number(e).toFixed(b), c])
                }
                if (e <= k) {
                    break
                }
                e -= d
            }
            g += j[f][1]
        }
        return h
    },
    getBlock: function(a, c) {
        if (a > c) {
            return c
        } else {
            c = c / 10;
            return this.getBlock(a, c)
        }
    },
    getZeros: function(b) {
        var a = "";
        while (b > 0) {
            b--;
            a += "0"
        }
        return a
    },
    getPrice: function(a, d) {
        var c = a[0];
        if (d == c) {
            c = "<h>" + c + ".</h>"
        } else {
            c += "."
        }
        var b = "";
        if (a.length == 1) {
            c += "0";
            b = this.getZeros(this.priceDecimalDigits - 1)
        } else {
            c += a[1];
            b = this.getZeros(this.priceDecimalDigits - a[1].length)
        }
        return [c, b]
    },
    getAmount: function(a) {
        var c = a[0];
        var b = "";
        var d = this.amountDecimalDigits - c.length + 1;
        if (d > 0) {
            b = ".";
            if (a.length == 1) {
                b += this.getZeros(d)
            } else {
                if (d > a[1].length) {
                    b += a[1] + this.getZeros(d - a[1].length)
                } else {
                    if (d == a[1].length) {
                        b += a[1]
                    } else {
                        b += a[1].substring(0, d)
                    }
                }
            }
        }
        return [c, b]
    },
    setTopTickers: function(c) {
        if (!c) {
            return
        }
        for (var a = 0; a < c.length; a++) {
            var b = c[a];
            if (b.moneyType == 0 && b.exeByRate == 1) {
                $("#markettop li." + b.symbol).find("span").text(b.ticker.dollar)
            } else {
                $("#markettop li." + b.symbol).find("span").text(b.ticker.last)
            }
        }
    },
    setMarketShow: function(e, b, d, c) {
        var a = e + "  " + (b + "/" + d).toUpperCase();
        $("#market a:eq(0)").attr("href", c).attr("title", a).text(a);
        if (this.isBtc123()) {
            $("#markettop li.order_info a").hide();
            $("#markettop li.depth_info a").hide()
        } else {
            $("#markettop li.order_info a").show().attr("href", "http://www.btc123.com/order?symbol=" + this.symbol);
            $("#markettop li.depth_info a").show().attr("href", "http://www.btc123.com/order/order?symbol=" + this.symbol)
        }
    },
    refreshPage: function(a) {
        if (a) {
            window.location.href = this.basePath + "/market?symbol=" + a
        } else {
            window.location.href = this.basePath + "/market"
        }
    },
    refreshUrl: function(a) {
        try {
            this.browerState++;
            $("#countView").find("iframe").attr("src", "https://www.btc123.com/kline/marketCount/" + a + "?symbol=" + a);
            History.pushState({
                state: this.browerState
            }, this.title, "?symbol=" + a)
        } catch (b) {}
    },
    isBtc123: function() {
        if (this.symbol.indexOf("btc123") >= 0) {
            return true
        } else {
            return false
        }
    }
};

/*function keepalive(a) {
    var b = new Date().getTime();
    if (a.bufferedAmount == 0) {
        a.send("{time:" + b + "}")
    }
};*/

function getQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}