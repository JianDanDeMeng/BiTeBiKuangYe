<?php
namespace Admin\Controller;
use Admin\Controller\AdminController;
use Think\Page;

class BitypeController extends AdminController {
    //空操作
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
	
	
	public function index(){
     $arr = $this->usubstr($_GET['type'],0);
      $arr1 = str_replace(".","",$arr);
	  if($arr1=='BTC'){
	  $arr1 = str_replace(".","",$arr).'+';
	  }
	  
	
       $currency = M('currency')->where(array('currency_mark'=>$arr1))->find();
	   
	
	    
        $_SESSION['currency_id'] = $currency['currency_id'];
        $map['c.currency_id'] = $currency['currency_id'];
        if($_POST){
            $map['c.member_id'] = $_POST['member_id'];
            $map['c.currency_id'] = $_POST['currency_id'];
        }
        $currency_user = M('currency_user c'); // 实例化User对象
        $count      = $currency_user->where($map)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $currency_user
            ->where($map)
            ->limit($Page->firstRow.','.$Page->listRows)
            ->join('yang_member m ON m.member_id = c.member_id')
            ->field('c.*,m.phone')
            ->select();

        $this->assign('currency',$currency);// 赋值数据集
        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出

        $this->display();
	}


    //交易记录
    public function trade_list(){

        $currency = M('currency')->where(array('currency_id'=>$_SESSION['currency_id']))->find();
        $map['c.currency_id'] = $currency['currency_id'];
        if($_POST['type']){
            $map['c.type'] = $_POST['type'];
        }
        if($_POST['member_id']){
            $map['c.member_id'] = $_POST['member_id'];
        }

        if($_POST['add_time']){
            $add_time = strtotime(I('post.add_time', ''));
            $a_end_time = I('post.end_time', '');
            $end_time = strtotime(I('post.end_time', '')) + 24*3600;

            if (!empty(I('post.add_time', '')) && !empty($a_end_time)){
                $map["c.add_time"] = array('between', array($add_time, $end_time));
            }
        }


        $map['c.currency_id'] = $_SESSION['currency_id'];
        $currency_user = M('trade c'); // 实例化User对象
        $count      = $currency_user->where($map)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $currency_user
            ->where($map)
            ->limit($Page->firstRow.','.$Page->listRows)
            ->join('yang_member m ON m.member_id = c.member_id')
            ->field('c.*,m.phone')
            ->select();
        $this->assign('add_time',time());
        $this->assign('end_time',time());
        $this->assign('currency',$currency);// 赋值数据集
        $this->assign('list',$list);// 赋值数据集
        $this->assign('page',$show);// 赋值分页输出

        $this->display();
    }

    /**
     * 完整词的截取
     *
     * @param $str
     * @param $start
     * @param $length
     *
     * @return string
     */
    public static function usubstr($str, $start, $length = null)
    {

        // 先正常截取一遍.
        $res = substr($str, $start, $length);
        $strlen = strlen($str);

        /* 接着判断头尾各6字节是否完整(不残缺) */
        // 如果参数start是正数
        if ($start >= 0) {
            // 往前再截取大约6字节
            $next_start = $start + $length; // 初始位置
            $next_len = $next_start + 6 <= $strlen ? 6 : $strlen - $next_start;
            $next_segm = substr($str, $next_start, $next_len);
            // 如果第1字节就不是 完整字符的首字节, 再往后截取大约6字节
            $prev_start = $start - 6 > 0 ? $start - 6 : 0;
            $prev_segm = substr($str, $prev_start, $start - $prev_start);
        } // start是负数
        else {
            // 往前再截取大约6字节
            $next_start = $strlen + $start + $length; // 初始位置
            $next_len = $next_start + 6 <= $strlen ? 6 : $strlen - $next_start;
            $next_segm = substr($str, $next_start, $next_len);

            // 如果第1字节就不是 完整字符的首字节, 再往后截取大约6字节.
            $start = $strlen + $start;
            $prev_start = $start - 6 > 0 ? $start - 6 : 0;
            $prev_segm = substr($str, $prev_start, $start - $prev_start);
        }
        // 判断前6字节是否符合utf8规则
        if (preg_match('@^([x80-xBF]{0,5})[xC0-xFD]?@', $next_segm, $bytes)) {
            if (!empty($bytes[1])) {
                $bytes = $bytes[1];
                $res .= $bytes;
            }
        }
        // 判断后6字节是否符合utf8规则
        $ord0 = ord($res[0]);
        if (128 <= $ord0 && 191 >= $ord0) {
            // 往后截取 , 并加在res的前面.
            if (preg_match('@[xC0-xFD][x80-xBF]{0,5}$@', $prev_segm, $bytes)) {
                if (!empty($bytes[0])) {
                    $bytes = $bytes[0];
                    $res = $bytes . $res;
                }
            }
        }
        if (strlen($res) < $strlen) {
            $res = $res . '...';
        }
        return $res;
    }




}