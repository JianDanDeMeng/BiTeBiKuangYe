<?php
namespace Admin\Controller;
use Common\Controller\CommonController;
class LoginController extends CommonController {
    //空操作
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
    public function login(){
        $this->display();
    }
    // 说明：获取完整URL

    public function curPageURL()
    {
        $pageURL = 'http';

        if ($_SERVER["HTTPS"] == "on")
        {
            $pageURL .= "s";
        }
        $pageURL .= "://";

        if ($_SERVER["SERVER_PORT"] != "80")
        {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        }
        else
        {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
    //加日志记录
    public function rizhi($arr, $member_id, $url){
        $str = '传递数据';
        foreach ($arr as $k => $v) {
            $str .= '---'.$v;
        }
        $str .= '传递地址'.'---'.$url;
        $str .= '传递账号UID'.'---'.$member_id;
        $str .= '所在IP'.'---'.get_client_ip();
        $str .= '传递时间'.'---'.date('Y-m-d H:i:s')."\r\n";
        // dump($str);die;
        $file_txt = fopen('/jylog/'.date('Y-m-d').'ad.txt', "a+");
        // dump($file_txt);die;
        fwrite($file_txt, $str);
    }
    //登录验证
    public function checkLogin(){
      //  $this->rizhi($_REQUEST, session('USER_KEY_ID'), $this->curPageURL());

            $str= date('Y-m-d H:i:s').'--shijian--'.$_SERVER["REMOTE_ADDR"].' --ip用户ID--'.I('post.username').'--paw--'.I('post.pwd')."\r\n";
        $file_pointer = fopen("./jylog/".date('Y-m-d').'htdl.'.txt,"a+");       
        fwrite($file_pointer,$str);
        
        $username=trim(I('post.username'));
        $pwd=trim(I('post.pwd'));
//        $phone=trim(I('post.phone'));
        if(empty($username)||empty($pwd)){
            $this->error('请填写完整信息');exit;
        }
        
         // if($_POST['code']=='' or $_POST['code']!=$_SESSION['code']){
         //     $this->error('验证码错误');exit;
         // }
        $mapu['username'] = $username;
        $admin=M('Admin')->where($mapu)->find();
        //var_dump($admin);
        if($admin['password']!=md5($pwd)){
            $this->error('登录密码不正确');exit;
        }
        
//         if($admin['phone']!=$phone){
//            $this->error('手机号不准确');exit;
//        }
        $_SESSION['admin_userid']=$admin['admin_id'];
        //var_dump($_SESSION);exit;
        
        $str= '是否登陆ID'.$_SESSION['admin_userid'].'---'.date('Y-m-d H:i:s').'--shijian--'.$_SERVER["REMOTE_ADDR"].' --ip用户ID--'.I('post.username').'--paw--'.I('post.pwd')."\r\n";
        $file_pointer = fopen("./jylog/".date('Y-m-d').'htdl.'.txt,"a+");       
        fwrite($file_pointer,$str);
        $_SESSION['code'] = '';
        $this->redirect('Index/index');
    }

  public function savecode(){
  
  $map['phone'] = $_POST['phone'];
  $data['code'] = $_SESSION['code'];
  M('Admin')->where($map)->save($data);
  }
  
  
    //登出
    public function loginout(){
        $_SESSION['admin_userid']=null;
        $this->redirect('Login/login');
    }
     
}