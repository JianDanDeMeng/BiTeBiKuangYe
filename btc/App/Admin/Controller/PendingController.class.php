<?php
/*
 * 后台审核提现
 */
namespace Admin\Controller;
use Admin\Controller\AdminController;
class PendingController extends AdminController {
	// 空操作
	public function _empty() {
		header ( "HTTP/1.0 404 Not Found" );
		$this->display ( 'Public:404' );
	}
	public function index() {
		$withdraw = M('withdraw');
		$bank = M ('bank');
		$keyname = I('get.keyname', '');
		$status = I('get.status', '');
		if($keyname){
			if(preg_match("/^\d*$/",$keyname)){
					$where['w.uid'] = $keyname;
			}else{
					$where['b.cardname'] = array('LIKE', "%{$keyname}%");
			}
		}
		
		$add_time = strtotime(I('get.add_time', ''));
		$a_end_time = I('get.end_time', '');
		$end_time = strtotime(I('get.end_time', '')) + 24*3600;
		if (!empty($add_time) && !empty($a_end_time))
		    $where["w.add_time"] = array('between', array($add_time, $end_time));
		// if (!empty($cardname))
		// 	$where['b.cardname'] = array('LIKE', "%{$cardname}%");
		// if (!empty($cardname))
		// 	$where['b.cardname'] = array('LIKE', "%{$keyname}%");
		if (!empty($status))
			$where['w.status'] = $status;
		$count = $withdraw
						->table('__WITHDRAW__ w')
						->join('__BANK__ b ON w.bank_id=b.id', 'LEFT')
						->where($where)
						->count();
		$Page = new \Think\Page ($count, 20);
		// 分页显示输出
		$show = $Page->show(); 
		$info = $withdraw
					->table('__WITHDRAW__ w')
					->field('w.*,b.cardname,b.bank_branch,b.cardnum,b.bankname,ab.area_name barea_name,ab.parent_id,a.area_name aarea_name,m.nick,m.email')
					->join('__BANK__ b ON w.bank_id=b.id', 'LEFT')
					->join('__AREAS__ ab ON ab.area_id=b.address', 'LEFT')
					->join('__AREAS__ a ON a.area_id=ab.parent_id', 'LEFT')
					->join('__MEMBER__ m ON m.member_id=w.uid', 'LEFT')
					->where($where)
					->group('w.withdraw_id')
					->order('w.status desc,w.add_time desc')
					->limit($Page->firstRow . ',' . $Page->listRows)
					->select();

/*
		// 查询满足要求的总记录数
		$count = $withdraw->join ( C("DB_PREFIX")."bank ON ".C("DB_PREFIX")."withdraw.bank_id = ".C("DB_PREFIX")."bank.id" )->where ( $where )->count (); 
		// 实例化分页类 传入总记录数和每页显示的记录数
		$Page = new \Think\Page ( $count, 20 ); 
		//将分页（点击下一页）需要的条件保存住，带在分页中
		$Page->parameter = array (
				C("DB_PREFIX")."withdraw.status" => $where [C("DB_PREFIX")."withdraw.status"],
				C("DB_PREFIX")."bank.cardname" =>  $cardname 
		);
		// 分页显示输出
		$show = $Page->show(); 
		//需要的数据
		$field = C("DB_PREFIX")."withdraw.*,".C("DB_PREFIX")."bank.cardname,".C("DB_PREFIX")."bank.cardnum,".C("DB_PREFIX")."bank.bankname,b.area_name as barea_name,a.area_name as aarea_name";
		$info = $bank->field ( $field )

				->join ( C("DB_PREFIX")."withdraw ON ".C("DB_PREFIX")."withdraw.bank_id = ".C("DB_PREFIX")."bank.id" )
				->join(C("DB_PREFIX")."areas as b ON b.area_id = ".C("DB_PREFIX")."bank.address")
				->join(C("DB_PREFIX")."areas as a ON a.area_id = b.parent_id ")
				->join("__PREFIX__member as m ON m.member_id= b.uid ", 'LEFT')

				->where ( $where )
				->order ( C("DB_PREFIX")."withdraw.status desc,".C("DB_PREFIX")."withdraw.add_time desc" )
				->limit ( $Page->firstRow . ',' . $Page->listRows )
				->select ();*/
				$tmp['add_time'] = I('get.add_time', '') ? $add_time : (M('withdraw')->order('add_time asc')->getField('add_time'));
				//找到提现的 结束时间
				$tmp['end_time'] = $a_end_time ? strtotime($a_end_time) : (M('withdraw')->order('add_time desc')->getField('add_time'));
				$this->assign('tmp', $tmp);

				$sup['add_time'] = date('Y-m-d', M('member')->order('member_id asc')->getField('reg_time'));
				//找到提现的 结束时间
				$sup['end_time'] = date('Y-m-d', M('member')->order('member_id desc')->getField('reg_time'));
				// dump($member->order('member_id asc')->getField('reg_time'));die;
				$this->assign('sup', $sup);

		$this->assign ( 'info', $info ); // 赋值数据集
		$this->assign ( 'page', $show ); // 赋值分页输出
		$this->assign ( 'inquire', $keyname );
		$this->display ();
	}

	/**
	 * 通过提现请求
	 * @param unknown $id
	 */
	public function successByid(){		
		$id = intval ( I ( 'post.id' ) );
			//判断是否$id为空
			if (empty ( $id ) ) {
				$datas['status'] = 3;
			    $datas['info'] = "参数错误";
			    $this->ajaxReturn($datas);
			}
		//查询用户可用金额等信息
		$info = $this->getMoneyByid($id);
		if($info['status']!=3){
			$datas['status'] = -1;
			$datas['info'] = "请不要重复操作";
			$this->ajaxReturn($datas);
		}
		//通过状态为2
		$data ['status'] = 2;
		$data ['check_time'] = time();
		$data ['admin_uid'] =$_SESSION['admin_userid'];
		//更新数据库
		$re = M ( 'Withdraw' )->where ( "withdraw_id = '{$id}'" )->save ( $data );	
		$num= M ( 'Withdraw' )->where ( "withdraw_id = '{$id}'" )->find ();
		M('Member')->where("member_id={$num['uid']}")->setDec('forzen_rmb',$num['all_money']);	
		if($re == false){
			$datas['status'] = 0;
			$datas['info'] = "提现操作失败";
			$this->ajaxReturn($datas);
		}
		$this->addMessage_all($info['member_id'],-2,'CNY提现成功',"恭喜您提现{$info['all_money']}成功！");
		$this->addFinance($info['member_id'],23,"提现{$info['all_money']}",$info['all_money']-$info['withdraw_fee'],2,0);
		$datas['status'] = 1;
		$datas['info'] = "提现通过，操作成功";
		$this->ajaxReturn($datas);
	}

	/**
	 * 不通过提现请求
	 * @param unknown $id
	 */
	public function falseByid(){
		$id = intval ( I ( 'post.id' ) );
			//判断是否$id为空
			if (empty ( $id ) ) {
				$this->error ( "参数错误" );
				return;
			}
		//查询用户可用金额等信息
		$info = $this->getMoneyByid($id);
		if($info['status']!=3){
			$datas['status'] = -1;
			$datas['info'] = "请不要重复操作";
			$this->ajaxReturn($datas);
		}
		//将提现的钱加回可用金额
		$money['rmb'] = floatval($info['rmb']) + floatval($info['all_money']);
		//将冻结的钱减掉
		$money['forzen_rmb'] = floatval($info['forzen_rmb']) - floatval($info['all_money']);
		
		//不通过状态为1
		$data ['status'] = 1;
		$data ['check_time'] = time();
		$data ['admin_uid'] =$_SESSION['admin_userid'];
		//更新数据库,member修改金额
		$res = M( 'Member' )->where("member_id = {$info['member_id']}")->save($money);
		//withdraw修改状态
		$re = M ( 'Withdraw' )->where ( "withdraw_id = '{$id}'" )->save ( $data );
		if($res == false){
			$datas['status'] = 0;
			$datas['info'] = "提现不通过，操作失败";
			$this->ajaxReturn($datas);
		}
		if($re == false){
			$datas['status'] = 2;
			$datas['info'] = "提现不通过，操作失败";
			$this->ajaxReturn($datas);
		}
		$this->addMessage_all($info['member_id'],-2,'CNY提现失败','很抱歉您提现失败，请重新操作或联系客服！');
		$datas['status'] = 1;
		$datas['info'] = "提现不通过，操作成功";
		$this->ajaxReturn($datas);
	}
	
	/**
	 * 获取提现金额信息
	 * @param unknown $id
	 * @return boolean|unknown $rmb 会员号，可用金额，冻结金额，手续费，提现金额
	 */
	public function getMoneyByid($id){

		$field = C("DB_PREFIX")."member.member_id,".C("DB_PREFIX")."member.rmb,".C("DB_PREFIX")."member.forzen_rmb,".C("DB_PREFIX")."withdraw.status,".C("DB_PREFIX")."withdraw.all_money,".C("DB_PREFIX")."withdraw.withdraw_fee";
		$rmb = M('Withdraw')
				->field($field)
				->join(C("DB_PREFIX")."member ON ".C("DB_PREFIX")."withdraw.uid = ".C("DB_PREFIX")."member.member_id")
				->where("withdraw_id = '{$id}'")
				->find();
		if(empty($rmb)){
			return false;
		}
		return $rmb;
	}
	/**
	 * 导出excel文件
	 */
	public function derivedExcel(){
		//时间筛选
		$add_time=I('get.add_time');
		$end_time=I('get.end_time');
		$add_time=empty($add_time)?0:strtotime($add_time);
		$end_time=empty($end_time)?0:strtotime($end_time);
		$where[C("DB_PREFIX").'withdraw.add_time'] = array('lt',$end_time);
		$withdraw = M('withdraw');
		$list = $withdraw
		->table('__WITHDRAW__ w')
		->field('
				w.withdraw_id,
				b.cardname,
				w.uid,
				b.bankname,
				b.cardnum,
				a.area_name aarea_name,
				ab.area_name barea_name,
				b.bank_branch,
				w.all_money,
				w.withdraw_fee,
				w.money,
				w.order_num,
				w.add_time,
				w.status')
		->join('__BANK__ b ON w.bank_id=b.id', 'LEFT')
		->join('__AREAS__ ab ON ab.area_id=b.address', 'LEFT')
		->join('__AREAS__ a ON a.area_id=ab.parent_id', 'LEFT')
		->join('__MEMBER__ m ON m.member_id=w.uid', 'LEFT')
		->where('w.add_time >'.$add_time)
		->select();
		foreach ($list as $k=>$v){
			$list[$k]['status']=drawStatus($v['status']);
			$list[$k]['add_time']=date('Y-m-d H:i:s',$v['add_time']);
			$list[$k]['cardnum']='`'.$list[$k]['cardnum'].'`';
		}
		$title = array(
				'Id',
				'提现人',
				'会员ID',
				'银行',
				'银行账号',
				'银行开户地',
				'银行开户地',
				'开户支行',
				'提现金额',
				'手续费',
				'实际金额',
				'订单号',
				'提交时间',
				'状态',
		);
		$filename= $this->config['name']."提现日志-".date('Y-m-d',time());
		$r = exportexcel($list,$title,$filename);
	}

	//代理商
	public function agent(){
			$member_id = I('member_id');	
			$status = I('status');
			if($member_id){
				$where['member_id'] = $member_id;
			}		
			if($status !== ''){
				$where['status'] = $status;
			}
			$pay = M('pay');$pay_log = M('pay_log');$trade = M('trade');
			$count = M('apply')->where($where)->count();
			$Page = new \Think\Page($count, 10);
			$show = $Page->show();
			$info = M('apply')->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('status asc,add_time desc')->select();
			if($info){
				foreach ($info as $key => $val) {
						$val['name'] = M('member')->where(array('member_id'=>$val['member_id']))->getField('name');
						$val['phone'] = M('member')->where(array('member_id'=>$val['member_id']))->getField('phone');
						//找到该用户下属会员的个数
						$val['lower_num'] = M('member')->where(array('pid'=>$val['member_id']))->count();
						$val['per_chongzhi_all'] = $pay->where(array('member_id'=>$val['member_id'], 'status'=>1))->sum('money') + $pay_log->where(array('uid'=>$val['member_id'], 'state'=>2))->sum('money');
						$val['per_trade_all'] = $trade->where(array('member_id'=>$val['member_id']))->sum('money');
						//找到该用户下属会员的充值金额
						$lower_arr = M('member')->field('member_id')->where(array('pid'=>$val['member_id']))->select();
						foreach ($lower_arr as $k => $v) {
							$val['chongzhi_all'] += $pay->where(array('member_id'=>$v['member_id'], 'status'=>1))->sum('money') + $pay_log->where(array('uid'=>$v['member_id'], 'state'=>2))->sum('money');
							$val['trade_all'] += $trade->where(array('member_id'=>$v['member_id']))->sum('money');
						}
						$res[] = $val;
				}	
			}
			$this->assign('res', $res);
			$this->assign('page', $show);
			$this->display();
		}

		public function ajax_agent_ok(){
			$member_id = intval(I('post.id'));
			M('member')->where('member_id='.$member_id)->setField('agent', 1);
			M('apply')->where('member_id='.$member_id)->setField('status', 1);
			$this->addMessage_all($member_id, -2, '成功成为代理中心', '成功成为 嗨币网代理中心');
			$data['status'] = 1;
			$data['info'] = '已成为代理中心';
			$this->ajaxReturn($data);
		}
		public function ajax_agent_fail(){
			$member_id = intval(I('post.id'));
			M('member')->where('member_id='.$member_id)->setField('agent', 0);
			M('apply')->where('member_id='.$member_id)->setField('status', 2);
			$data['status'] = 2;
			$data['info'] = '已拒绝成为代理中心';
			$this->ajaxReturn($data);
		}
		public function ajax_cancel(){
			$member_id = intval(I('post.id'));
			M('member')->where('member_id='.$member_id)->setField('agent', 0);
			M('apply')->where('member_id='.$member_id)->setField('status', 3);
			$this->addMessage_all($member_id, -2, '取消代理中心','系统取消了您的代理中心资格');
			$data['status'] = 1;
			$data['info'] = '已取消该客户的代理中心资格';
			$this->ajaxReturn($data);
		}

		public function sell_fee(){
			$member_id = I('member_id');
			if($member_id) $where['member_id'] = $member_id;
			//得到上个月的 卖出手续费记录
			//得到上个月第一天的时间戳
			$before_time = strtotime(date('Y-m-01', strtotime('-1 month')));
			//得到上个月最后一天的时间戳
			$after_time = strtotime(date('Y-m-t', strtotime('-1 month')));
			$where['add_time'] = array('between', array($before_time, $after_time));
			$where['type'] = 1;
			$count = M('invit')->where($where)->group('member_id')->count();
			$Page = new \Think\Page($count, 10);
			$show = $Page->show();
			$info = M('invit')->field('*,sum(money) as sum_all')->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('status asc,add_time desc')->group('member_id')->select();
			foreach ($info as $key => $val) {
				$temp = M('member')->field('name, phone')->where(array('member_id'=>$val['member_id']))->find();
				$val['name'] = $temp['name'];
				$val['phone'] = $temp['phone'];
				$res[] = $val;
			}
			$this->assign('res', $res);
			$this->assign('page', $show);
			$this->display();
		}
		public function buy_fee(){
			$member_id = I('member_id');
			if($member_id) $where['member_id'] = $member_id;
			//得到上个月的 卖出手续费记录
			//得到上个月第一天的时间戳
			$before_time = strtotime(date('Y-m-01', strtotime('-1 month')));
			//得到上个月最后一天的时间戳
			$after_time = strtotime(date('Y-m-t', strtotime('-1 month')));
			$where['add_time'] = array('between', array($before_time, $after_time));
			$where['type'] = 2;
			$count = M('invit')->where($where)->group('member_id')->count();
			$Page = new \Think\Page($count, 10);
			$show = $Page->show();
			$info = M('invit')->field('*,sum(money) as sum_all')->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('status asc,add_time desc')->group('member_id')->select();
			foreach ($info as $key => $val) {
				$temp = M('member')->field('name, phone')->where(array('member_id'=>$val['member_id']))->find();
				$val['name'] = $temp['name'];
				$val['phone'] = $temp['phone'];
				$res[] = $val;
			}
			$this->assign('res', $res);
			$this->assign('page', $show);
			$this->display();
		}

		public function ajax_give_ok(){
			$member_id_arr = I('id');
			$member_id_arr = (array)$member_id_arr;
			$type = I('type');
			//得到上个月第一天的时间戳
			$before_time = strtotime(date('Y-m-01', strtotime('-1 month')));
			//得到上个月最后一天的时间戳
			$after_time = strtotime(date('Y-m-t', strtotime('-1 month')));
			$where['add_time'] = array('between', array($before_time, $after_time));
			if($member_id_arr) $where['member_id'] = array('in', $member_id_arr);
			$where['type'] = $type;
			$where['status'] = 0;
			$res = M('invit')->where($where)->select();
			foreach ($res as $key => $val) {
				M('member')->where(array('member_id'=>$val['member_id']))->setInc('rmb', $val['money']);
			}
			M('invit')->where($where)->setField('status', 1);
			$this->ajaxReturn('发放成功');
		}

		public function collect(){
				$member = M('member');$pay = M('pay');$pay_log = M('pay_log');$trade = M('trade');
				$invit = M('invit');$withdraw = M('withdraw');$get_vaojifen = M('get_vaojifen');
				$keyname = I('get.keyname', '');
				$add_time = strtotime(I('get.add_time', ''));
				$a_end_time = I('get.end_time', '');
				$end_time = strtotime(I('get.end_time', '')) + 24*3600;
				if (!empty($add_time) && !empty($a_end_time)){
					$loop_where["add_time"] = array('between', array($add_time, $end_time));
					$loop_new_where["addtime"] = array('between', array($add_time, $end_time));
					$t_end_time = I('get.end_time');
					$t_end_time = date('Y-m-d', strtotime("$t_end_time +1 day"));
					strtotime("2016-11-07 +1 day");
					$loop_in_where["c_ctime"] = array('between', array(I('get.add_time'), $t_end_time));
				} 
				if($keyname){
					if(preg_match("/^\d*$/",$keyname)){
							$where['member_id'] = $keyname;
							// $loop_where["member_id"] = $keyname;
							// $loop_new_where['member_id'] = $keyname;
					}else{
							$where['name'] = array('LIKE', "%{$keyname}%");
							// $loop_where['member_id'] = $member->where()->getField('member_id');
							// $loop_new_where['member_id'] = $member->where()->getField('member_id');
					}
				}
				$count = $member->where($where)->count();
				$Page = new \Think\Page($count, 10);
				$show = $Page->show();
				$info = $member->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->select();
				foreach ($info as $key => $val) {
					$val['per_in'] = $get_vaojifen->where($loop_in_where)->where(array('u_id'=>$val['member_id']))->sum('sum');
					$val['per_chongzhi_all'] = $pay->where($loop_where)->where(array('member_id'=>$val['member_id'], 'status'=>1))->sum('money') + $pay_log->where($loop_new_where)->where(array('uid'=>$val['member_id'], 'state'=>2))->sum('money');
					$val['per_trade_buy_kld'] = $trade->where($loop_where)->where(array('member_id'=>$val['member_id'], 'type'=>'buy', 'currency_id'=>'34'))->sum('money');
					$val['per_trade_sell_kld'] = $trade->where($loop_where)->where(array('member_id'=>$val['member_id'], 'type'=>'sell', 'currency_id'=>'34'))->sum('money');
					$val['per_trade_buy_xtb'] = $trade->where($loop_where)->where(array('member_id'=>$val['member_id'], 'type'=>'buy', 'currency_id'=>'35'))->sum('money');
					$val['per_trade_sell_xtb'] = $trade->where($loop_where)->where(array('member_id'=>$val['member_id'], 'type'=>'sell', 'currency_id'=>'35'))->sum('money');
					$val['per_withdraw'] = $withdraw->where($loop_where)->where(array('uid'=>$val['member_id'], 'status'=>'2'))->sum('money');
					$val['give_fee'] = $invit->where($loop_where)->where(array('member_id'=>$val['member_id'], 'status'=>1))->sum('money');
					$res[] = $val;
				}
				$tmp['add_time'] = I('get.add_time', '') ? $add_time : ($member->order('member_id asc')->getField('reg_time'));
				//找到提现的 结束时间
				$tmp['end_time'] = $a_end_time ? strtotime($a_end_time) : ($member->order('member_id desc')->getField('reg_time'));
				// dump($member->order('member_id asc')->getField('reg_time'));die;
				$this->assign('tmp', $tmp);
				$this->assign('res', $res);
				$this->assign('page', $show);
				$this->display ();
		}

		//大矿机审核
		public function big_check(){
				$member_id = I('member_id');	
				$status = I('status');
				if($member_id){
					$where['member_id'] = $member_id;
				}		
				if($status !== ''){
					$where['status'] = $status;
				}
				// $where['type'] = 1;
				$pay = M('pay');$pay_log = M('pay_log');$trade = M('trade');$member = M('member');
				$count = M('mac_orders')->where($where)->count();
				$Page = new \Think\Page($count, 10);
				$show = $Page->show();
				$info = M('mac_orders')->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('status asc,add_time desc')->select();
				if($info){
					foreach ($info as $key => $val) {
							$val['name'] = $member->where(array('member_id'=>$val['member_id']))->getField('name');
							$val['phone'] = $member->where(array('member_id'=>$val['member_id']))->getField('phone');
							//找到该用户下属会员的个数
							$val['lower_num'] = $member->where(array('pid'=>$val['member_id']))->count();
							$val['per_chongzhi_all'] = $pay->where(array('member_id'=>$val['member_id'], 'status'=>1))->sum('money') + $pay_log->where(array('uid'=>$val['member_id'], 'state'=>2))->sum('money');
              $val['per_trade_buy_kld'] = $trade->where(array('member_id'=>$val['member_id'], 'type'=>'buy', 'currency_id'=>'34'))->sum('money');
              $val['per_trade_sell_kld'] = $trade->where(array('member_id'=>$val['member_id'], 'type'=>'sell', 'currency_id'=>'34'))->sum('money');
              $val['per_trade_buy_xtb'] = $trade->where(array('member_id'=>$val['member_id'], 'type'=>'buy', 'currency_id'=>'35'))->sum('money');
							$val['per_trade_sell_xtb'] = $trade->where(array('member_id'=>$val['member_id'], 'type'=>'sell', 'currency_id'=>'35'))->sum('money');
							$res[] = $val;
					}	
				}
				$this->assign('res', $res);
				$this->assign('page', $show);
				$this->display();
			}

			public function ajax_big_check_ok(){
				$mac_orders_id = intval(I('post.id'));
				$member_id = M('mac_orders')->where(array('mac_orders_id'=>$mac_orders_id))->getField('member_id');
				$in_money = M('mac_orders')->where(array('mac_orders_id'=>$mac_orders_id))->getField('all_money');
				$theType = M('mac_orders')->where(array('mac_orders_id'=>$mac_orders_id))->getField('type');
				if($theType == 1){
					//成为代理中心
					// M('member')->where('member_id='.$member_id)->setField('agent', 1);
					//改变购买状态
					M('mac_orders')->where(array('mac_orders_id'=>$mac_orders_id))->setField('status', 2);
					//扣冻结钱
					M('member')->where('member_id='.$member_id)->setDec('forzen_rmb', $in_money);
					$this->addMessage_all($member_id, -2, '购买大矿机审核通过', '成功购买大矿机');
				}else{
					//改变购买状态
					M('mac_orders')->where(array('mac_orders_id'=>$mac_orders_id))->setField('status', 2);
					//扣冻结钱
					M('member')->where('member_id='.$member_id)->setDec('forzen_rmb', $in_money);
					$this->addMessage_all($member_id, -2, '购买小矿机审核通过', '成功购买小矿机');
				}
				
				$data['status'] = 1;
				$data['info'] = '审核通过';
				$this->ajaxReturn($data);
			}
			public function ajax_big_check_fail(){
				$mac_orders_id = intval(I('post.id'));
				$member_id = M('mac_orders')->where(array('mac_orders_id'=>$mac_orders_id))->getField('member_id');
				$in_money = M('mac_orders')->where(array('mac_orders_id'=>$mac_orders_id))->getField('all_money');
				//成为代理中心
				// M('member')->where('member_id='.$member_id)->setField('agent', 1);
				//改变购买状态
				M('mac_orders')->where(array('mac_orders_id'=>$mac_orders_id))->setField('status', 3);
				//扣冻结钱
				M('member')->where('member_id='.$member_id)->setDec('forzen_rmb', $in_money);
				M('member')->where('member_id='.$member_id)->setInc('rmb', $in_money);
				$this->addMessage_all($member_id, -2, '购买大矿机审核失败', '很遗憾, 购买大矿机审核失败');
				$data['status'] = 1;
				$data['info'] = '审核未通过';
				$this->ajaxReturn($data);
			}

			public function giveCoin(){
				if(IS_POST){
					$member_id = I('post.member_id');
					$currency_id = I('post.currency_id');
					$num = I('post.num');
					if(!M('member')->where(array('member_id'=>$member_id))->find()){
						$this->error('该会员不存在');exit;
					}

					if(!(is_numeric($num) && $num > 0)){
						$this->error('请输入正确的转出数量');exit;
					}
					require_once 'App/Common/Common/easybitcoin.php';
					//龙厦币
					if($currency_id == 34){
						$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','101.201.121.247','8024');
						$info = $this->getUserMoneyByCurrencyIds($member_id, $currency_id);
						$re = $member_id.$currency_id;
						if(empty($info['chongzhi_url'])){
							$address = $bitcoin->getaccountaddress($re);
							$this->setCurrentyMemberByMemberIds($member_id, $currency_id, 'chongzhi_url', $address);
						}
						$bitcoin->move('', $re, floatval($num));
						$this->setUserMoney($member_id, $currency_id, $num , 'inc', 'num');
					}
					//星特币
					if($currency_id == 35){
						$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
						$info = $this->getUserMoneyByCurrencyIds($member_id, $currency_id);
						$re = $member_id.$currency_id;

						if(empty($info['chongzhi_url'])){
							$address = $bitcoin->getaccountaddress($re);
							$this->setCurrentyMemberByMemberIds($member_id, $currency_id, 'chongzhi_url', $address);
						}

						$bitcoin->move('8888', $re, floatval($num));
						$this->setUserMoney($member_id, $currency_id, $num , 'inc', 'num');
					}
					$this->success('转出成功');exit;
				}
				$this->display();
			}

			public function  setCurrentyMemberByMemberIds($member_id,$currenty_id,$key,$value){
				return   M("Currency_user")->where("member_id=$member_id and  currency_id=$currenty_id")->setField($key,$value);
			}
			//获取个人账户指定币种金额
			public function getUserMoneyByCurrencyIds($member_id,$currency_id){
				return M('Currency_user')->field('num,forzen_num,chongzhi_url')->where(array('member_id'=>$member_id, 'currency_id'=>$currency_id))->find();
			}

			public function ceshi(){
				$sel = I('member_id') . '35';
				require_once 'App/Common/Common/easybitcoin.php';
				$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
				echo $bitcoin->getbalance($sel);
			}
			public function move(){
				$sel = I('member_id') . '35';
				$num = floatval(I('num'));
				require_once 'App/Common/Common/easybitcoin.php';
				$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
				$bitcoin->move('8888', $sel, $num);
			}
			public function mem_move(){
				$sel = I('member_id') . '35';
				$num = floatval(I('num'));
				require_once 'App/Common/Common/easybitcoin.php';
				$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
				$bitcoin->move($sel, '8888', $num);
			}
			public function last_move(){
				$sel = I('member_id') . '35';
				$lower = I('tmember_id') . '35';
				$num = floatval(I('num'));
				require_once 'App/Common/Common/easybitcoin.php';
				$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
				$bitcoin->move($sel, $lower, $num);
			}

			public function kmove(){
				$sel = I('member_id') . '34';
				$num = floatval(I('num'));
				require_once 'App/Common/Common/easybitcoin.php';
				$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','101.201.121.247','8024');
				$bitcoin->move('', $sel, $num);
			}

			public function kceshi(){
				$sel = I('member_id') . '34';
				require_once 'App/Common/Common/easybitcoin.php';
				$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','101.201.121.247','8024');
				echo $bitcoin->getbalance($sel);
			}

			public function kmem_move(){
				$sel = I('member_id') . '34';
				$num = floatval(I('num'));
				require_once 'App/Common/Common/easybitcoin.php';
				$bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','101.201.121.247','8024');
				$bitcoin->move($sel, '', $num);
			}

			/**
			 * 统计全站信息
			 */
			public function infoStatistics(){
			    //统计全站信息
			    //总人数
			    $member_count=M('Member')->count();
			    //代理中心总数
			    $theAgent = M('member')->where(array('agent'=>1))->count();
			    $this->assign('theAgent', $theAgent);
			    //人工充值CNY总额
			    $per_chong = M('pay')->where(array('status'=>1))->sum('money');
			    $this->assign('per_chong', $per_chong);
			    // 在线充值CNY总额
			    $line_chong = M('pay_log')->where(array('state'=>2))->sum('money');
			    $this->assign('line_chong', $line_chong);
			    // 会员CNY可用余额
			    $rmb = M('member')->sum('rmb');
			    $this->assign('rmb', $rmb);
			    // 会员CNY冻结总额
			    $forzen_rmb = M('member')->sum('forzen_rmb');
			    $this->assign('forzen_rmb', $forzen_rmb);
			    //全站币种统计
			    $currency_u_info = M('currency')
			                    ->alias('a')
			                    ->field('a.currency_name,sum(b.num) as num,sum(b.forzen_num) as forzen_num')
			                    ->join("left join ".C("DB_PREFIX")."currency_user AS b on a.currency_id = b.currency_id")
			                    ->group('a.currency_id')
			                    ->select();
			    $this->assign('member',$member_count);
			    // $this->assign('issue_count',$issue_count);
			    // $this->assign('pay_money_count',$pay_money_count);
			    // $this->assign('withdraw_money_count',$withdraw_money_count);
			    // $this->assign('pay_count',$pay_count);
			    // $this->assign('withdraw_count',$withdraw_count);
			    $this->assign('currency_u_info',$currency_u_info);
			    $this->display();
			}

}
