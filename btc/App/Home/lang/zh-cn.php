<?php
return array(

	//全局
	'language' => 'cn',
	'lzwjw' => '嗨币网',
	'login' => '登录',
	'password' => '密码',
	'lost_password' => '忘记密码？',
	'sign_up_for_free' => '免费注册',
	'year' => '年',
	'month' => '月',
	'day' => '日',
	'comingsoon' => '敬请期待',
	'virtual_currency' => '虚拟货币',
	'more' => '更多',

	//页头
	'add2favorite' => '加入收藏',
	'sethomepage' => '设置首页',
	'servicetel' => '客服电话',
	'sign_in' => '你好，请登陆',
	'sign_up' => '注册',
	'welcome' => '欢迎回来，',
	'log_out' => '安全退出',

	//导航条
	'navbar_home' => '首页',
	'navbar_trade_center' => '交易大厅',
	'btc' => '比特币',
	'ltc' => '莱特币',
	'kld_coin' => '龙厦币',
	'xtb' => '星特盾',
	'navbar_crowdfunding' => '众筹',
	'navbar_market_center' => '行情中心',
	'navbar_news' => '最新动态',
	'navbar_my_wallet' => '我的资产',
	'navbar_log_in' => '立即登录',
	'navbar_my_homepage' => '我的主页',

	//首页
	'email_telphone' => '请输入邮箱或已认证的手机号',
	'btn_login' => '立即登录',
	'wjinwang_market' => '嗨币网行情',


	//钱款方面
	'account_balance' => '可用余额',
	'CNY' => '美元',
	'CNY_' => '美元：',
	'blocked_funds' => '委托冻结',
	'recharge' => '充值',
	'withdraw' => '提现',
	'entrust_manage' => '委托管理',
	'transaction_manage' => '成交查询',

	//行情部分
	'daily_change' => '24H涨跌',
	'week_change' => '7D涨跌',
	'daily_turnover' => '24h成交量',
	'daily_total_volume' => '24h成交额',

	'latest' => '最新价',
	'low' => '日最低价',
	'high' => '日最高价',
	'total_vol' => '总市值',
	'change' => '涨跌幅',
	'daily_volume' => '24H成交金额',

	'trade_now' => '立即交易',
	'order_book' => '买卖盘',
	'tradable' => '可交易量',
	'sell' => '卖',
	'buy' => '买',
	'live_trades' => '实时成交',
	'amount' => '成交量',

	//首页公告栏
	'announcements' => '网站公告',
	'market_information' => '市场动态',
	'information' => '业内资讯',

	//首页统计
	'statistics_title' => '选择嗨币网，安全可信赖',
	'total_amount' => '累计交易额',

	//首页四联信息
	'tips_title' => '专业技术团队为您的数字资产交易保驾护航',
	'tips_a_title' => '系统可靠',
	'tips_a_desc' => '银行级用户数据加密<br/>动态身份验证<br/>多级风险识别控制<br/>保障交易安全',
	'tips_b_title' => '资金安全',
	'tips_b_desc' => '钱包多层加密<br/>离线存储于银行保险柜<br/>资金第三方托管<br/>确保安全',
	'tips_c_title' => '快捷方便',
	'tips_c_desc' => '充值即时、提现迅速<br/>每秒万单的高性能交<br/>易引擎<br/>保证一切快捷方便',
	'tips_d_title' => '服务专业',
	'tips_d_desc' => '专业的客服团队<br/>400电话和在线QQ<br/>VIP一对一专业服务',

	//首页友情链接
	'partner' => '合作伙伴',

	//页脚
	'footer_l_title' => '关于',
	'footer_l_event' => '大事记',
	'footer_l_about' => '关于我们',
	'footer_l_team' => '技术团队',

	'footer_c_help_title' => '帮助中心',
	'footer_c_help_reg' => '注册指南',
	'footer_c_help_trade' => '交易指南',
	'footer_c_help_recharge' => '充值指南',
	'footer_c_help_transfer' => '转币指南',
	'footer_c_help_tools' => '工具下载',
	'footer_c_help_creidits_kld' => '区块查询(嗨币网龙厦币)',
	'footer_c_help_creidits_xtd' => '区块查询(星特盾)',

	'footer_r_contact_title' => '联系我们',
	'footer_r_contact_mail' => '客服邮箱：',
	'footer_r_contact_cooperate' => '业务合作：',
	'footer_r_contact_complain' => '投诉邮箱：',



	



);
?>