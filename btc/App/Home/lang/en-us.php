<?php
return array(

	//全局
	'language' => 'en',
	'lzwjw' => '',
	'login' => 'Sign in',
	'password' => 'Password',
	'lost_password' => 'forgot your password?',
	'sign_up_for_free' => 'Free register',
	'year' => 'year',
	'month' => 'month',
	'day' => 'day',
	'comingsoon' => 'Coming soon',
	'virtual_currency' => 'virtual currency',

	//页头
	'add2favorite' => 'Add to favorite',
	'sethomepage' => 'Set homepage',
	'servicetel' => 'Service Telphone',
	'sign_in' => 'Sign in',
	'sign_up' => 'Register',
	'welcome' => 'Welcome back,',
	'log_out' => 'Log out',

	//导航条
	'navbar_home' => 'Home',
	'navbar_trade_center' => 'Trade center',
	'btc' => 'Bitcoin',
	'ltc' => 'Litecoin',
	'kld_coin' => 'Konladcoin',
	'navbar_crowdfunding' => 'Crowdfunding',
	'navbar_market_center' => 'Market center',
	'navbar_news' => 'News',
	'navbar_my_wallet' => 'My wallet',
	'navbar_log_in' => 'Sign in',
	'navbar_my_homepage' => 'My page',

	//首页
	'email_telphone' => 'Email/Phone number',
	'btn_login' => 'Sign in',
	'wjinwang_market' => 'Lezhong Market',


	//钱款方面
	'account_balance' => 'Account Balance',
	'CNY' => 'CNY',
	'CNY_' => 'CNY: ',
	'blocked_funds' => 'Blocked Funds',
	'recharge' => 'Recharge',
	'withdraw' => 'Withdraw',
	'entrust_manage' => 'Entrust Manage',
	'transaction_manage' => 'Transaction Manage',

	//行情部分
	'daily_change' => 'Daily Change',
	'week_change' => 'Week Change',
	'daily_turnover' => 'Daily Turnover',
	'daily_total_volume' => 'Daily Total Volume',

	'latest' => 'Latest',
	'low' => 'Low',
	'high' => 'High',
	'total_vol' => 'Total Vol.',
	'change' => 'Change',
	'daily_volume' => 'Daily Vol.',

	'trade_now' => 'Trade now',
	'order_book' => 'Order book',
	'tradable' => 'Tradable',
	'sell' => 'Sell',
	'buy' => 'Buy',
	'live_trades' => 'Live trades',
	'amount' => 'Amount',

	//首页公告栏
	'announcements' => 'Announcements',
	'market_information' => 'Market',
	'information' => 'Information',

	//首页统计
	'statistics_title' => 'Choose us, secure and trustworthy',
	'total_amount' => 'Total amount',

	//首页四联信息
	'tips_title' => 'Professional technical team to ensure your financial transactions',
	'tips_a_title' => 'Trustworthy',
	'tips_a_desc' => 'Bank level dynamic user data encryption authentication Multi-level risk identification control to ensure the safety of trading',
	'tips_b_title' => 'Security',
	'tips_b_desc' => 'Wallet multilayer encryption offline store money in the bank safe escrow To ensure safety',
	'tips_c_title' => 'Convenient',
	'tips_c_desc' => 'Top-up real-time, rapid withdrawal per second than single high-performance engine Ensure all quick and convenient',
	'tips_d_title' => 'Professional',
	'tips_d_desc' => 'Professional customer service team of 400 telephone and online QQ VIP one-on-one professional services',

	//首页友情链接
	'partner' => 'Partners',

	//页脚
	'footer_l_title' => 'About',
	'footer_l_event' => 'Event',
	'footer_l_about' => 'About us',
	'footer_l_team' => 'Team',

	'footer_c_help_title' => 'Help',
	'footer_c_help_reg' => 'Reg guide',
	'footer_c_help_trade' => 'Trade guide',
	'footer_c_help_recharge' => 'How to recharge',
	'footer_c_help_transfer' => 'Transfer guide',
	'footer_c_help_tools' => 'Tools',
	'footer_c_help_creidits' => 'Creidits',

	'footer_r_contact_title' => 'Contact us',
	'footer_r_contact_mail' => 'Service email: ',
	'footer_r_contact_cooperate' => 'Cooperate: ',

	



);
?>