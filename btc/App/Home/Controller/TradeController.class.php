<?php
namespace Home\Controller;
use Home\Controller\TradeFatherController;
class TradeController extends TradeFatherController {
    protected  $currency;
    protected  $entrust;
    protected  $trade;
    protected  $err = array();
    protected  $unOff = array();
    protected  $once = array();
    protected  $minus = array();
    public function _initialize(){
        parent::_initialize();

        $this->currency=M('Currency');
        $this->entrust=M('Entrust');
        $this->trade=M('Orders');
    }
    //空操作
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }

    //获取个人账户指定币种金额
    public function getUserMoneyByCurrencyIds($user,$currencyId){
        return M('Currency_user')->field('num,forzen_num,chongzhi_url')->where("Member_id={$this->member['member_id']} and currency_id=$currencyId")->find();
    }
    
    protected function  setCurrentyMemberByMemberIds($member_id,$currenty_id,$key,$value){
        return   M("Currency_user")->where("member_id=$member_id and  currency_id=$currenty_id")->setField($key,$value);
    }

    // 判断当前时间是否在 交易时间段
    public function jiaoyi_time($currency_id){

        $r = M('currency')->where(array('currency_id'=>$currency_id))->find();
        $w = date('w');
        if($w == 0 and $r['day_qi']==2) return false;
        if($w == 1 and $r['day_yi']==2) return false;
        if($w == 2 and $r['day_er']==2) return false;
        if($w == 3 and $r['day_san']==2) return false;
        if($w == 4 and $r['day_si']==2) return false;
        if($w == 5 and $r['day_wu']==2) return false;
        if($w == 6 and $r['day_liu']==2) return false;

        $r['am_start_time']= strtotime(date('Y-m-d')) + $r['am_start_time']*3600;
        $r['am_end_time']= strtotime(date('Y-m-d')) + $r['am_end_time']*3600;
        $r['pm_start_time']= strtotime(date('Y-m-d')) + $r['pm_start_time']*3600;
        $r['pm_end_time']= strtotime(date('Y-m-d')) + $r['pm_end_time']*3600;

        if(($r['am_start_time']<=time() && time()<=$r['am_end_time']) || ($r['pm_start_time']<=time() && time()<=$r['pm_end_time'])){
            return true;
        }else{
            return false;
        }
    }

    //买入
    public function buy(){

        $del['member_id'] = cookie('USER_KEY_ID');
        $del['sha'] =$_COOKIE['SHA_LOG'];
        $deng_log =  M('denglu_log')->where($del)->find();
        if(!$deng_log){
             cookie('USER_KEY_ID' ,null);
        }

        if(!$this->checkLogin()){
            $data['status']=0;
            $data['info']='请先登录再进行此操作';
            $this->ajaxReturn($data);
        }

        if(!$this->jiaoyi_time(intval(I('post.currency_id')))){
            $data['status']=-10;
            $data['info']='交易未开启，请在交易时间内进行交易。';
            $this->ajaxReturn($data);
        }

        $buyprice=floatval(I('post.buyprice'));
        $buynum=floatval(I('post.buynum'));
        $buypwd=I('post.buypwd');
        $buycurrency_id=intval(I('post.currency_id'));
        
        //xw
        $listb=$this->getUserMoneyByCurrencyIds($_COOKIE['USER_KEY_ID'], $buycurrency_id);
        //设置充值地址
//        if(empty($listb['chongzhi_url'])){
//            require_once 'App/Common/Common/easybitcoin.php';
//            //莱特币
//            if($buycurrency_id==26){
//                $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','114.215.44.55','9148');
//            }
//            //嗨币网龙厦币
//            if($buycurrency_id==34){
//                $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','101.201.121.247','8024');
//            }
//            //星特盾
//            if($buycurrency_id==35){
//                $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
//            }
//            //比特币
//            if($buycurrency_id==28){
//                $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','101.201.121.247','8332');
//            }
//
//                $re = $_COOKIE['USER_KEY_ID'].$buycurrency_id;
//                $address = $bitcoin->getaccountaddress($re);
//                $this->setCurrentyMemberByMemberIds($_COOKIE['USER_KEY_ID'], $buycurrency_id, 'chongzhi_url', $address);
//        }
        
        
        //获取币的相关信息
        $currency=$this->getCurrencyByCurrencyId($buycurrency_id);
        if ($currency['is_lock']){
            $data['status']=-5;
            $data['info']= '该币种暂时不允许交易';
            $this->ajaxReturn($data);
        }

        if(!is_numeric($buyprice)||!is_numeric($buynum)){
            $data['status']=0;
            $data['info']='您的挂单价格或数量有误,请修改';
            $this->ajaxReturn($data);
        }
        //涨停价格限制
        if ($currency['price_down']>0&&$buyprice<$currency['price_down']){
            $msg['status']=-9;
            $msg['info']='交易价格超出了跌停价格限制';
            $this->ajaxReturn($msg);
        }

        //涨停价格限制
        if ($currency['price_up']>0&&$buyprice>$currency['price_up']){
            $msg['status']=-7;
            $msg['info']='交易价格超出了涨停价格限制';
            $this->ajaxReturn($msg);
        }

        if ($buyprice*$buynum<1){
            $data['status']=0;
            $data['info']='不能委托低于1元的订单';
            $this->ajaxReturn($data);
        }

        if (!preg_match('/^[0-9]+(.[0-9]{1,4})?$/', $buynum)) {  
            $data['status']=-1;
            $data['info']='非法输入';
            $this->ajaxReturn($data); 
        }
        
        if ($buynum<0.0001){
            $data['status']=-2;
            $data['info']='交易数量必须是正数';
            $this->ajaxReturn($data);
        }
        $member=$this->member;
        if(cookie('PWDS_ID')!=cookie('USER_KEY_ID') or cookie('PWDS_ID')=='') {
            if (md5(I('post.buypwd')) != $member['pwdtrade']) {
                $data['status'] = -3;
                $data['info'] = '交易密码不正确';
                $this->ajaxReturn($data);
            }
        }
        cookie('PWDS_ID',cookie('USER_KEY_ID'));
        if ($this->checkUserMoney($buynum, $buyprice, 'buy', $currency)){
            $data['status']=-4;
            $data['info']='您账户余额不足';
            $this->ajaxReturn($data);
        }
        if(M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->getField('rmb') < -2){
            $data['status']=-11;
            $data['info']='您账户可用美元小于0, 不能挂单, 请联系客服';
            $this->ajaxReturn($data);
        }
        if(M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->getField('forzen_rmb') < -2){
            $data['status']=-11;
            $data['info']='您账户冻结美元小于0, 不能挂单, 请联系客服';
            $this->ajaxReturn($data);
        }
      //  M()->query('lock tables yang_orders write, yang_currency_user write');
        //开启事物
        M()->startTrans();

        //计算买入需要的金额
        $trade_money=$buynum*$buyprice;//*(1+($currency['currency_buy_fee']/100));
        
        //操作账户
        $this->setUserMoney($this->member['member_id'],$currency['trade_currency_id'], $trade_money,'dec', 'num');
        $this->setUserMoney($this->member['member_id'],$currency['trade_currency_id'], $trade_money, 'inc','forzen_num');

        // $this->jilu($buynum, $buyprice, 'buy', $currency);

            //挂单流程
        $this->guadan($buynum, $buyprice, 'buy', $currency);    

       //交易流程
      $r = $this->trade($currency['currency_id'], 'buy', $buynum, $buyprice);

      $flag = $this->deep_in_array('false', $r);
 //      M()->query("UNLOCK TABLES");
       if ($flag){
           M()->rollback();
           if(!empty($this->minus)){
                foreach ($this->minus as $key => $val) {
                    $tmp = M('orders')->where(array('member_id'=>$val, 'status'=>0, 'currency_id'=>$buycurrency_id))->select();
                    foreach ($tmp as $k => $v) {
                        $this->cancelOrdersByOrderId($v);
                    }
                }
           }
           $msg['status']=-7;
           $msg['info']='操作未成功';
           $this->ajaxReturn($msg);
       }else {
           M()->commit();
           $msg['status']=1;
           $msg['info']='操作成功';
           $this->ajaxReturn($msg);
       }
    }

    /*卖出
     *
     * 1.是否登录
     * 1.5 是否开启交易
     * 2.准备数据
     * 3.判断数据
     * 4.检查账户
     * 5.操作个人账户
     * 6.写入数据库
     *
     *
     *
     */

    public function sell(){
        $del['member_id'] = cookie('USER_KEY_ID');
        $del['sha'] =$_COOKIE['SHA_LOG'];
        $deng_log =  M('denglu_log')->where($del)->find();
        if(!$deng_log){
             cookie('USER_KEY_ID' ,null);
        }
        if(!$this->checkLogin()){
            $data['status']=-1;
            $data['info']='请先登录再进行此操作';
            $this->ajaxReturn($data);
        }

        
        if(!$this->jiaoyi_time(intval(I('post.currency_id')))){
            $data['status']=-10;
            $data['info']='交易未开启，请在交易时间内进行交易。';
            $this->ajaxReturn($data);
        }

        $sellprice=floatval(I('post.sellprice'));
        $sellnum=floatval(I('post.sellnum'));
        $sellpwd=I('post.sellpwd');
        $currency_id=I('post.currency_id');
        
        //xw
        $lists=$this->getUserMoneyByCurrencyIds($_COOKIE['USER_KEY_ID'], $currency_id);
//        //设置充值地址
//        if(empty($lists['chongzhi_url'])){
//            require_once 'App/Common/Common/easybitcoin.php';
//            //莱特币
//            if($currency_id==26){
//                $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','114.215.44.55','9148');
//            }
//            //嗨币网龙厦币
//            if($currency_id==34){
//                $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','101.201.121.247','8024');
//            }
//            //星特币
//            if($currency_id==35){
//                $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
//            }
//            //比特币
//            if($currency_id==28){
//                $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','101.201.121.247','8332');
//            }
//
//            $re = $_COOKIE['USER_KEY_ID'].$currency_id;
//            $address = $bitcoin->getaccountaddress($re);
//            $this->setCurrentyMemberByMemberIds($_COOKIE['USER_KEY_ID'], $currency_id, 'chongzhi_url', $address);
//        }
        
        //获取币种信息
        $currency=$this->getCurrencyByCurrencyId($currency_id);
        //检查是否开启交易
       if ($currency['is_lock']){
           $msg['status']=-2;
           $msg['info']='该币种暂时不能交易';
           $this->ajaxReturn($msg);
       }

        if (empty($sellprice)||empty($sellnum)){
            $msg['status']=-3;
            $msg['info']='卖出价格或在数量不能为空';
            $this->ajaxReturn($msg);
        }

        if ($sellnum*$sellprice<1){
            $data['status']=0;
            $data['info']='不能委托低于1元的订单';
            $this->ajaxReturn($data);
        }

        if (empty($sellpwd)){
            $msg['status']=-4;
            $msg['info']='交易密码不能为空';
            $this->ajaxReturn($msg);
        }
        if(cookie('PWDS_ID')!=cookie('USER_KEY_ID') or cookie('PWDS_ID')=='') {
            if ($this->member['pwdtrade'] != md5($sellpwd)) {
                $msg['status'] = -5;
                $msg['info'] = '交易密码不正确';
                $this->ajaxReturn($msg);
            }
        }
        cookie('PWDS_ID',cookie('USER_KEY_ID'));
         if ($sellnum<0.0001){
            $data['status']='';
            $data['info']='非法数据';
            $this->ajaxReturn($data);
        }
        
        
        if (!preg_match('/^[0-9]+(.[0-9]{1,4})?$/', $sellnum)) {  
            $data['status']='';
            $data['info']='非法输入';
            $this->ajaxReturn($data); 
        }
        
        //涨停价格限制
        if ($currency['price_down']>0&&$sellprice<$currency['price_down']){
            $msg['status']=-9;
            $msg['info']='交易价格超出了跌停价格限制';
            $this->ajaxReturn($msg);
        }

        //涨停价格限制
        if ($currency['price_up']>0&&$sellprice>$currency['price_up']){
            $msg['status']=-7;
            $msg['info']='交易价格超出了涨停价格限制';
            $this->ajaxReturn($msg);
        }

        //检查账户是否有钱
        if ($this->checkUserMoney($sellnum, $sellprice, 'sell', $currency)){
            $msg['status']=-6;
            $msg['info']='您的账户余额不足';
            $this->ajaxReturn($msg);
        }

        if(M('currency_user')->where(array('member_id'=>cookie('USER_KEY_ID'), 'currency_id'=>$currency_id))->getField('num') < -2){
            $msg['status']=-11;
            $msg['info']='您的账户可用币小于0, 不能挂单, 请联系客服';
            $this->ajaxReturn($msg);
        }
        if(M('currency_user')->where(array('member_id'=>cookie('USER_KEY_ID'), 'currency_id'=>$currency_id))->getField('forzen_num') < -2){
            $msg['status']=-11;
            $msg['info']='您的账户冻结币小于0, 不能挂单, 请联系客服';
            $this->ajaxReturn($msg);
        }

        //减可用钱 加冻结钱
        M()->startTrans();
        $this->setUserMoney($this->member['member_id'],$currency['currency_id'], $sellnum, 'dec', 'num');
        $this->setUserMoney($this->member['member_id'],$currency['currency_id'], $sellnum,'inc','forzen_num');
        // $this->jilu($sellnum, $sellprice, 'sell', $currency);
        //写入数据库
        $this->guadan($sellnum, $sellprice, 'sell', $currency);
        //成交
        $r = $this->trade($currency['currency_id'], 'sell', $sellnum, $sellprice);

        $flag = $this->deep_in_array('false', $r);

        if ($flag){
            M()->rollback();
            if(!empty($this->minus)){
                 foreach ($this->minus as $key => $val) {
                     $tmp = M('orders')->where(array('member_id'=>$val, 'status'=>0, 'currency_id'=>$currency_id))->select();
                     foreach ($tmp as $k => $v) {
                         $this->cancelOrdersByOrderId($v);
                     }
                 }
            }
            $msg['status']=-7;
            $msg['info']='操作未成功';
            $this->ajaxReturn($msg);
        }else {
            M()->commit();
            $msg['status']=1;
            $msg['info']='操作成功';
            $this->ajaxReturn($msg);
        }
    }



    //我的成交
    public function myDeal(){
        if (!$this->checkLogin()){
            $this->redirect(U('Index/index','',false));
        }
      //获取主币种
        $currency=$this->getCurrencyByCurrencyId();
        $this->assign('culist',$currency);
        $currencytype = I('currency');

        if(!empty($currencytype)){
            $where['currency_id'] =$currencytype;
        }
        $where['member_id'] = $_COOKIE['USER_KEY_ID'];

        $count      = M('Trade')->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数

        //给分页传参数
        setPageParameter($Page, array('currency'=>$currencytype));

        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = M('Trade')->where($where)->limit($Page->firstRow.','.$Page->listRows)->order('trade_id desc')->select();
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('list',$list);
        $this->display();
    }



    /**
     *
     * @param int  $num 数量
     * @param float $price 价格
     * @param char $type 买buy 卖sell
     * @param $currency_id 交易币种
     */
        private function  checkUserMoney($num,$price,$type,$currency){

            //获取交易币种信息
            if ($type=='buy'){
                $trade_money=$num*$price*(1+$currency['currency_buy_fee']/100);
                $currency_id=$currency['trade_currency_id'];
            }else {
                $trade_money=$num;
                $currency_id=$currency['currency_id'];
            }
            //和自己的账户做对比 获取账户信息
            $money=$this->getUserMoney($currency_id, 'num');
            if ($money<$trade_money){
                return true;
            }else{
                return false;
            }
        }

    /**
     * 挂单
     * @param int  $num 数量
     * @param float $price 价格
     * @param char $type 买buy 卖sell
     * @param $currency_id 交易币种
     */
    private function guadan($num,$price,$type,$currency){
        //获取交易币种信息
        switch ($type){
            case 'buy':
                    $fee=$currency['currency_buy_fee'];
                    $currency_trade_id=$currency['trade_currency_id'];
                    break;
            case 'sell':
                $fee=$currency['currency_sell_fee'];
                $currency_trade_id=$currency['trade_currency_id'];
                break;
        }
        $data=array(
            'member_id'=>$this->member['member_id'],
            'currency_id'=>$currency['currency_id'],
            'currency_trade_id'=>$currency['trade_currency_id'],
            'price'=>$price,
            'num'=>$num,
            'trade_num'=>0,
            'fee'=>$fee,
            'type'=>$type,
        );
        if (D('Orders')->create($data)){
          $msg=D('Orders')->add();

        }else {
            $msg=0;
        }

        return $msg;

    }

    private function jilu($num,$price,$type,$currency){
        //获取交易币种信息
        switch ($type){
            case 'buy':
                    $fee=$currency['currency_buy_fee'];
                    $currency_trade_id=$currency['trade_currency_id'];
                    break;
            case 'sell':
                $fee=$currency['currency_sell_fee'];
                $currency_trade_id=$currency['trade_currency_id'];
                break;
        }

        $vos = array(
        'member_id'=>$this->member['member_id'],
        'currency_id'=>$currency['currency_id'],
        'price'=>$price,
        'num'=>$num,
        'money'=>$num*$price,
        'fee'=>$fee,
        'type'=>$type,
                'status'=>1,
                'time'=>time()
              );
            return M('forzen_log')->add($vos);

    }



    private function trade($currencyId,$type,$num,$price){
        $theOrders = M('orders');
        $theMember = M('member');
        $currency_user = M('currency_user');
        if ($type=='buy'){
            $trade_type='sell';
        }else {
            $trade_type='buy';
        }
        $memberId=$_COOKIE['USER_KEY_ID'];
        //获取操作人一个订单
        $order=$this->getFirstOrdersByMember($memberId,$type ,$currencyId);
        //获取对应交易的一个订单
        $trade_order=$this->getOneOrders($trade_type, $currencyId,$price);

       //如果没有相匹配的订单，直接返回
        if (empty($trade_order)){
            $r[]='true';
            return $r;
        }
        //如果有就处理订单
        $trade_num=min($num,$trade_order['num']-$trade_order['trade_num']);
        $trade_num >= 0 ? $r[] = 'true' : $r[] = 'false';

        //增加本订单的已经交易的数量
        $theOrders->where("orders_id={$order['orders_id']}")->setInc('trade_num',$trade_num);
        $theOrders->where("orders_id={$order['orders_id']}")->setField('trade_time',time());
        //增加trade订单的已经交易的数量
        $theOrders->where("orders_id={$trade_order['orders_id']}")->setInc('trade_num',$trade_num);
        $theOrders->where("orders_id={$trade_order['orders_id']}")->setField('trade_time',time());

        //更新一下订单状态
        // $theOrders->where("trade_num>0 and status=0")->setField('status',1);
        $theOrders->where("num=trade_num")->setField('status',2);

        //处理资金
        switch ($type){

            case 'buy':
                $trade_price=min($order['price'],$trade_order['price']);
                $trade_price >= 0 ? $r[] = 'true' : $r[] = 'false';
                $trade_order_money= $trade_num*$trade_price*(1-($trade_order['fee']/100));
                $order_money= $trade_num*$trade_price;//*(1+$order['fee']);
                
                //这是给买的人加
                $this->setUserMoney($this->member['member_id'],$order['currency_id'], $trade_num*(1-($order['fee']/100)), 'inc', 'num');

                $this->setUserMoney($this->member['member_id'],$order['currency_trade_id'], $order_money, 'dec', 'forzen_num');

                $this->setUserMoney($trade_order['member_id'],$trade_order['currency_id'],  $trade_num, 'dec', 'forzen_num');
                $this->setUserMoney($trade_order['member_id'],$trade_order['currency_trade_id'], $trade_order_money, 'inc', 'num');

                //返还多扣除的部分
                $this->setUserMoney($this->member['member_id'],$order['currency_trade_id'], $trade_num*($order['price']-$trade_price), 'inc', 'num');
                $this->setUserMoney($this->member['member_id'],$order['currency_trade_id'], $trade_num*($order['price']-$trade_price), 'dec', 'forzen_num');

                // 该用户买入 给父级代理和爷级代理手续费
                $this->setBuyInvitMoney($order['currency_id'], $order['currency_trade_id'], $this->member['member_id'], $trade_num, $trade_price);
                //该用户卖出 给二级代理和总代理
                $this->setSellInvitMoney($trade_order['currency_id'], $trade_order['currency_trade_id'], $trade_order['member_id'], $trade_num, $trade_price);
                break;
            case 'sell':
                $trade_price=max($order['price'],$trade_order['price']);
                $trade_price > 0 ? $r[] = 'true' : $r[] = 'false';
                $order_money= $trade_num*$trade_price*(1-($order['fee']/100));
                $trade_order_money= $trade_num*$trade_price;
                
                $this->setUserMoney($this->member['member_id'],$order['currency_id'], $trade_num, 'dec', 'forzen_num');
                $this->setUserMoney($this->member['member_id'],$order['currency_trade_id'], $order_money, 'inc', 'num');

                $this->setUserMoney($trade_order['member_id'],$trade_order['currency_id'], $trade_num*(1-($trade_order['fee']/100)), 'inc', 'num');
                $this->setUserMoney($trade_order['member_id'],$trade_order['currency_trade_id'],$trade_order_money, 'dec', 'forzen_num');

                //trade_order 买入 给父级代理和爷级代理手续费
                $this->setBuyInvitMoney($trade_order['currency_id'], $trade_order['currency_trade_id'], $trade_order['member_id'], $trade_num, $trade_price);
                 //order 卖出 给父级代理和爷级代理手续费
                $this->setSellInvitMoney($order['currency_id'], $order['currency_trade_id'], $this->member['member_id'], $trade_num, $trade_price);
                break;
        }

        //修正最终成交的价格
        $theOrders->where("num=trade_num and orders_id={$order['orders_id']}")->setField('price',$trade_price);
        $theOrders->where("num=trade_num and orders_id={$trade_order['orders_id']}")->setField('price',$trade_price);
        
        //写入成交表
        $this->addTrade($order['member_id'], $order['currency_id'], $order['currency_trade_id'],$trade_price, $trade_num, $order['type'], $order['fee'], $trade_order['member_id']);
        $this->addTrade($trade_order['member_id'], $trade_order['currency_id'], $trade_order['currency_trade_id'], $trade_price, $trade_num, $trade_order['type'],$trade_order['fee'], $order['member_id']);   
        
        $num =$num- $trade_num;

        if($theMember->where("member_id='{$order['member_id']}'")->getField('forzen_rmb') >= -2){
            $r[] = 'true';
        }else{
            $r[] = 'false';
            $this->minus[] = $order['member_id'];
        }

        if($theMember->where("member_id='{$trade_order['member_id']}'")->getField('forzen_rmb') >= -2){
            $r[] = 'true';
        }else{
            $r[] = 'false';
            $this->minus[] = $trade_order['member_id'];
        }

        if($currency_user->where(array('member_id'=>$order['member_id'], 'currency_id'=>$order['currency_id']))->getField('forzen_num') >= -2){
            $r[] = 'true';
        }else{
            $r[] = 'false';
            $this->minus[] = $order['member_id'];
        }

        if($currency_user->where(array('member_id'=>$trade_order['member_id'], 'currency_id'=>$order['currency_id']))->getField('forzen_num') >= -2){
            $r[] = 'true';
        }else{
            $r[] = 'false';
            $this->minus[] = $trade_order['member_id'];
        }

        if ($num>0){
            //递归
           $r[] = $this->trade($currencyId, $type, $num, $price);
        }

        return $r;

    }


    public function setBuyInvitMoney($currency_id, $currency_trade_id, $member_id, $trade_num, $trade_price){
        // 判断是否为星特盾
        if($currency_id != 35) return;
        if(time() > '1490198400') return;
        //判断该用户是否有 pid
        $pid = M('member')->where(array('member_id'=>$member_id))->getField('pid');
        if(!$pid) return;
        //判断 pid是不是代理商
        if(!M('member')->where(array('member_id'=>$pid, 'agent'=>1))->find()) return;
        //该用户给上级手续费 最大值
        $max_sum = M('maxfee')->where(array('member_id'=>$memebr_id))->getField('max_sum');
        //该用户已给上级 总手续费
        $give_sum = M('invit')->where(array('lower_id'=>$member_id))->sum('money');
        if($give_sum > $max_sum) return;
        $data = array(
                'member_id'=>$pid,
                'lower_id'=>$member_id,
                'type'=>2,
                'currency_id'=>$currency_id,
                'currency_trade_id'=>$currency_trade_id,
                'money'=>$trade_num*$trade_price*0.03,
                'add_time'=>time()
            );
        M('invit')->add($data);
    }


    public function setSellInvitMoney($currency_id, $currency_trade_id, $member_id, $trade_num, $trade_price){
        //判断是否为星特盾
        if($currency_id != 35) return;
        if(time() < '1490198400') return;
        //判断该用户是否有 pid
        $onePid = M('member')->where(array('member_id'=>$member_id))->getField('pid');
        if(!$onePid) return;
        //3月23号后 给父级代理 爷级代理手续费
        $one_lower_agent = M('member')->where(array('member_id'=>$onePid))->getField('lower_agent');
        $one_agent = M('member')->where(array('member_id'=>$onePid))->getField('agent');
        //如果 onePid不是父级代理 不是爷级代理
        if($one_lower_agent==0 && $one_agent==0) return;
        //如果 onePid不是父级代理 是爷级代理 直接把钱给爷级代理
        if($one_lower_agent==0 && $one_agent==1){
            $oneData = array(
                    'member_id'=>$onePid,
                    'lower_id'=>$member_id,
                    'type'=>1,
                    'currency_id'=>$currency_id,
                    'currency_trade_id'=>$currency_trade_id,
                    'money'=>$trade_num*$trade_price*0.005,
                    'add_time'=>time()
                );
            M('invit')->add($oneData);
            return;
        }
        //如果 onePid是父级代理 不是爷级代理  那twoPid必定是爷级代理
        if($one_lower_agent==1 && $one_agent==0){
            $oneData = array(
                    'member_id'=>$onePid,
                    'lower_id'=>$member_id,
                    'type'=>1,
                    'currency_id'=>$currency_id,
                    'currency_trade_id'=>$currency_trade_id,
                    'money'=>$trade_num*$trade_price*0.003,
                    'add_time'=>time()
                );
            M('invit')->add($oneData);
            $twoPid = M('member')->where(array('member_id'=>$onePid, 'agent'=>1))->getField('pid');
            if(!$twoPid) return;
            $twoData = array(
                    'member_id'=>$twoPid,
                    'lower_id'=>$member_id,
                    'type'=>1,
                    'currency_id'=>$currency_id,
                    'currency_trade_id'=>$currency_trade_id,
                    'money'=>$trade_num*$trade_price*0.002,
                    'add_time'=>time()
                );
            M('invit')->add($twoData);
            return;
        }
        //如果 onePid是父级代理 也是爷级代理
        if($one_lower_agent==1 && $one_agent==1){
            $twoPid = M('member')->where(array('member_id'=>$onePid))->getField('pid');
            //如果 twoPid不存在
            if(!$twoPid){
                $oneData = array(
                        'member_id'=>$onePid,
                        'lower_id'=>$member_id,
                        'type'=>1,
                        'currency_id'=>$currency_id,
                        'currency_trade_id'=>$currency_trade_id,
                        'money'=>$trade_num*$trade_price*0.005,
                        'add_time'=>time()
                    );
                M('invit')->add($oneData);
                return;
            }else{
                $two_agent = M('member')->where(array('member_id'=>$twoPid))->getField('agent');
                //twoPid是爷级代理
                if($two_agent == 1){
                    $oneData = array(
                            'member_id'=>$onePid,
                            'lower_id'=>$member_id,
                            'type'=>1,
                            'currency_id'=>$currency_id,
                            'currency_trade_id'=>$currency_trade_id,
                            'money'=>$trade_num*$trade_price*0.003,
                            'add_time'=>time()
                        );
                    M('invit')->add($oneData);
                    $twoData = array(
                            'member_id'=>$twoPid,
                            'lower_id'=>$member_id,
                            'type'=>1,
                            'currency_id'=>$currency_id,
                            'currency_trade_id'=>$currency_trade_id,
                            'money'=>$trade_num*$trade_price*0.002,
                            'add_time'=>time()
                        );
                    M('invit')->add($twoData);
                    return;
                }else{
                    $oneData = array(
                            'member_id'=>$onePid,
                            'lower_id'=>$member_id,
                            'type'=>1,
                            'currency_id'=>$currency_id,
                            'currency_trade_id'=>$currency_trade_id,
                            'money'=>$trade_num*$trade_price*0.005,
                            'add_time'=>time()
                        );
                    M('invit')->add($oneData);
                    return;
                }
            }
        }
    }

    /**
     * 返回一条挂单记录
     * @param int $currencyId 币种id
     * @param float $price 交易价格
     * @return array 挂单记录
     */
    private function getOneOrders($type,$currencyId,$price){
        switch ($type){
            case 'buy':$gl='egt';$order='price desc,add_time asc'; break;
            case 'sell':$gl='elt'; $order='price asc,add_time asc';break;
        }
        $where['currency_id']=$currencyId;
        $where['type']=$type;
        $where['price']=array($gl,$price);
        $where['status']=0;
        
        return M('Orders')->where($where)->order($order)->find();
    }
    /**
     * 返回用户第一条未成交的挂单
     * @param int $memberId 用户id
     * @param int $currencyId 币种id
     * @return array 挂单记录
     */
    private function getFirstOrdersByMember($memberId,$type,$currencyId){
        $where['member_id']=$memberId;
        $where['currency_id']=$currencyId;
        $where['type']=$type;
        $where['status']=0;
        return M('Orders')->where($where)->order('add_time desc')->find();
    }
    /**
     *  返回指定价格排序的订单
     * @param char $type  buy sell
     * @param float $price   交易价格
     * @param char $order 排序方式
     */
    private function getOrdersByPrice($currencyId,$type,$price,$order){
        switch ($type){
            case 'buy': $gl='elt';break;
            case 'sell': $gl='egt';break;
        }
        $where['currency_id']=$currencyId;
        $where['price']=array($gl,$price);
        $where['status']=0;
        return  M('Orders')->where($where)->order("price  $order")->select();
    }


    /**
     * 增加交易记录
     * @param unknown $member_id
     * @param unknown $currency_id
     * @param unknown $currency_trade_id
     * @param unknown $price
     * @param unknown $num
     * @param unknown $type
     * @return boolean
     */
    private function  addTrade($member_id,$currency_id,$currency_trade_id,$price,$num,$type,$fee,$trade_member_id){
        switch ($type){
            case 'buy':$fee=$num*$fee*0.01;break;
            case 'sell':$fee=$price*$num*$fee*0.01;break;
            default:$fee=0;
        }
        // $this->dividend($price*$num,$member_id);
        $data=array(
            'member_id'=>$member_id,
            'currency_id'=>$currency_id,
            'currency_trade_id'=>$currency_trade_id,
            'price'=>$price,
            'num'=>$num,
            'fee'=>$fee,
            'money'=>$price*$num,
            'type'=>$type,
            'trade_member_id'=>$trade_member_id
        );
        if (D('Trade')->create($data)){
            if (D('Trade')->add()){
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

// 递归判断多维数组 是否存在某个值
    public function deep_in_array($value, $array) { 
            foreach($array as $item) { 
                    if(!is_array($item)) { 
                        if ($item == $value) {
                            return true;
                        } else {
                            continue; 
                }
            } 

                if(in_array($value, $item)) {
                        return true; 
                    } else if($this->deep_in_array($value, $item)) {
                        return true; 
                }
        } 
        return false; 
    }

}