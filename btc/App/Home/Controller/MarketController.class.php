<?php
/**
 */

namespace Home\Controller;

use Common\Controller\CommonController;

class MarketController extends CommonController {
    /**
     * 查看行情
     */
  public function index(){

      if(!empty($_POST['currency'])){
        $whereMark['currency_mark'] = I('get.currency');
      }

      $whereMark['is_line']=1;
      $liCurrency = M('Currency')->field('currency_id,currency_name,currency_logo,currency_mark')->where($whereMark)->order('sort')->find();

      //判断 有没有 可以交易的币种
      if(empty($liCurrency)){
        $this -> error('交易币种正在紧张筹备中！敬请期待',U('Index/index'));

      }

      $this ->assign('count',max(count($sell),count($buy)));
      $this ->assign('deal',$Deal);
      $this ->assign('sell',$sell);
      $this ->assign('buy',$buy);
      $this ->assign('listCurrency',$listCurrency1);
        $currency_id=I('get.currency');
        $currency=M('Currency')->where("currency_mark='$currency_id' and is_line=1")->find();
        if(empty($currency)){
            $this->display('Public:b_stop');
            return;
        }
        $currency['currency_digit_num']=$currency['currency_digit_num']?$currency['currency_digit_num']:4;//设置限制位数
        //显示委托记录
        $buy_record=$this->getOrdersByType($currency['currency_id'],'buy', 10, 'desc');
        $sell_record=$this->getOrdersByType($currency['currency_id'],'sell', 10, 'asc');
        $sell_record = array_reverse($sell_record);
        $this->assign('buy_record',$buy_record);
        $this->assign('sell_record',$sell_record);
        //格式化手续费
        $currency['currency_sell_fee']=floatval($currency['currency_sell_fee']);
        $currency['currency_buy_fee']=floatval($currency['currency_buy_fee']);
        //币种信息
        $currency_message=$this->getCurrencyMessageById($currency['currency_id']);
        $currency_trade=$this->getCurrencynameById($currency['trade_currency_id']);
        $this->assign('currency_message',$currency_message);
        $this->assign('currency_trade',$currency_trade);
        //个人账户资产
        if (!empty($_COOKIE['USER_KEY_ID'])){
            $user_currency_money['currency']['num']=$this->getUserMoney($currency['currency_id'], 'num');
            $user_currency_money['currency']['forzen_num']=$this->getUserMoney($currency['currency_id'], 'forzen_num');
            $user_currency_money['currency_trade']['num']=$this->getUserMoney($currency['trade_currency_id'], 'num');
            $user_currency_money['currency_trade']['forzen_num']=$this->getUserMoney($currency['trade_currency_id'], 'forzen_num');
            if($currency['trade_currency_id']==0){
                $user_currency_money['currency_trade']['num']=$this->member['rmb'];
                $user_currency_money['currency_trade']['forzen_num']=$this->member['forzen_rmb'];
            }
            $this->assign('user_currency_money',$user_currency_money);
            //个人挂单记录
            $this->assign('user_orders',$this->getOrdersByUser(5,$currency['currency_id']));
            //最大可买
            if (!empty($sell_record)){
              $buy_num=sprintf('%.4f',$user_currency_money['currency_trade']['num']/$sell_record[0]['price']);
            }else {
                $buy_num=0;
            }
            $this->assign('buy_num',$buy_num);
            //最大可卖
            $sell_num=sprintf('%.4f',$user_currency_money['currency']['num']);
            $this->assign('sell_num',$sell_num);
        }
        $this->assign('cookie',$_COOKIE['USER_KEY_ID']);
        $this->assign('currency',$currency);
        //成交记录
        $trade=$this->getOrdersByStatus(2, 20, $currency['currency_id']);
        $this->assign('trade',$trade);


      $this->display();
  }


  //买卖
  public function js_max_min(){
     $currency_id=$_GET['currency_id'];
       //买卖盘   买
      $buy=$this->getOrdersByType($currency_id, 'buy', 10, 'desc');
      $buy1=array();
      foreach ($buy as $v) {
        $buy1[]=array('p'=>$v['price'],'n'=>$v['num']-$v['trade_num']);
      }
      $liCurrency['buy'] = $buy1;
     // unset($buy1);
      //买卖盘   卖
       $sell =$this->getOrdersByType($currency_id, 'sell', 10, 'asc');
       $sell = array_reverse($sell);
      $sell1=array();
      foreach ($sell as $v) {
        $sell1[]=array('p'=>$v['price'],'n'=>$v['num']-$v['trade_num']);
      }
     $liCurrency['sale'] = $sell1;
      $this->ajaxReturn($liCurrency);

  }

   // /**
   //    * 返回指定数量排序的挂单记录
   //    * @param char $type buy sell
   //    * @param int $num 数量
   //    * @param char $order 排序 desc asc
   //    */
   //     public function getOrdersByTypes($currencyid,$type,$num,$order){
   //       $where['type']=array('eq',$type);
   //       $where['status']=0;
   //       $where['currency_id']=$currencyid;
   //       $list= M('Orders')->field("sum(num) as num,sum(trade_num) as trade_num,price,type,status")->where($where)->group('price')->order("price $order")->limit($num)->select();
   //       foreach ($list as $k=>$v){
   //           $list[$k]['bili']=100-($v['trade_num']/$v['num']*100);
   //       }
   //       if ($type=='sell'){
   //         $list=  array_reverse($list);
   //       }
   //       return $list;
   //   }

  //leez modify 20170105 start
  public function js_orders(){
      $date = strtotime(date("Y-m-d 00:00:00"));
      $wheretrade['currency_id'] = $_GET['currency_id'];
      //$liCurrency['d'] = M('orders')->where('status=2')->order('trade_time desc')->limit(30)->select();
      //获取成交最大值
      //$liCurrency['max'] = M('Trade') ->where($wheretrade)->max('price');
      //获取成交最小值
      //$liCurrency['min'] = M('Trade') ->where($wheretrade)->min('price');

      //获取今日最新价
      $liCurrency['max'] = M('Trade') ->where($wheretrade)->order('add_time desc')->getField('price');
      //获取昨日收盘价
      $liCurrency['min'] = M('Trade') ->where($wheretrade)->where('add_time<'.$date)->order('add_time desc')->getField('price');


      // 获取交易量
      $liCurrency['sum'] = M('Trade') ->where($wheretrade)->sum('num');
      $wheretrade['status'] = 2;
      $liCurrency['d'] = M('orders')->where($wheretrade)->order('trade_time desc')->limit(50)->select();

      $this->ajaxReturn($liCurrency);
  }
  //leez modify 20170105 end
  public function coin(){
    $where['is_line']=1;
      $listCurrency = $this->currency();
      $this->ajaxReturn($listCurrency);
  }
  public function newprice(){
    $where['is_line']=1;
    $where['is_lock']=0;
      $listCurrency = M('Currency')->field('currency_id,currency_mark')->where($where)->select();
      $a=array();
      foreach($listCurrency as $k =>$v){
       // $where1['status']=array('in',array(1,2));
       $where1['currency_id']=$v['currency_id'];
       $list=M('trade')->field('price')->where($where1)->order('add_time desc')->find();
       //var_dump($list);
        $a[$v['currency_mark']]= !empty($list['price'])?$list['price']:0;
      }
      $this->ajaxReturn($a);
  }

  //leez add 20161217
  public function nerv_orders(){
    $time = time();
    $date = strtotime(date("Y-m-d 00:00:00"));
    $buy = $sell = $rs = $re = $liCurrency = array();
    $wheretrade['currency_id'] = $_GET['currency_id'];

    $_message=$this->getCurrencyMessageById($wheretrade['currency_id']);

    //最新价格
    // $order='add_time desc';
    // $rs = M('Trade')->where('currency_id='.$wheretrade['currency_id'])->order($order)->find();
    $liCurrency['new_price'] = $_message['new_price'];
    $liCurrency['new_price_status'] = $_message['new_price_status'];
    $liCurrency['name'] = $this->getCurrencynameById($wheretrade['currency_id']);

    //买卖盘   买
    foreach ($this->getOrdersByType($wheretrade['currency_id'], 'buy', 5, 'desc') as $v) {
      $buy[]=array('price'=>$v['price'],'num'=>$v['num'],'trade_num'=>$v['trade_num']);
    }
    $liCurrency['buy'] = $buy;
    //买卖盘   卖
    foreach ($this->getOrdersByType($wheretrade['currency_id'], 'sell', 5, 'asc') as $v) {
      $sell[]=array('price'=>$v['price'],'num'=>$v['num'],'trade_num'=>$v['trade_num']);
    }
    $liCurrency['sell'] = $sell;
    //获取成交最大值
    $liCurrency['price_max'] = M('Trade') ->where($wheretrade)->where('add_time>'.$date)->max('price');
    //获取成交最小值
    $liCurrency['price_min'] = M('Trade') ->where($wheretrade)->where('add_time>'.$date)->min('price');
    // 获取交易量
    $liCurrency['sum'] = M('Trade') ->where($wheretrade)->sum('num');
    $wheretrade['status'] = 2;
    $liCurrency['d'] = M('orders')->where($wheretrade)->order('trade_time desc')->limit(10)->select();
    //24H涨跌幅
    $liCurrency['day_change']= $_message['24H_change'];
    //24H成交额
    $liCurrency['day_done_money'] = $_message['24H_done_money'];

    $this->ajaxReturn($liCurrency);
  }

}
