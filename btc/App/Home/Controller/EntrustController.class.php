<?php
namespace Home\Controller;
use Home\Controller\TradeFatherController;
class EntrustController extends TradeFatherController {
     public function _initialize(){
          if (empty($_COOKIE['USER_KEY_ID'])){
               $this->redirect('Login/login');
          }
          parent::_initialize();
     }
     //空操作
     public function _empty(){
          header("HTTP/1.0 404 Not Found");
          $this->display('Public:404');
     }
    //委托管理
     public function manage(){
         $this->User_status();//判断是否需要进行信息补全
          //获取主币种
          $currency=$this->getCurrencyByCurrencyId();
          $this->assign('culist',$currency);
          $currencytype = I('currency');
          $status=I('status');
          if(!empty($currencytype)){
               $where['currency_id'] =$currencytype;
          }
          
          $where['status'] = array('in','0,1');
          $where['member_id'] = $_COOKIE['USER_KEY_ID'];
          
          if(!empty($status) || $status === '0'){
               $where['status'] = $status;
          }
          
          $count      = M('Orders')->where($where)->count();// 查询满足要求的总记录数
          $Page       = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数
          
          //给分页传参数
          setPageParameter($Page, array('currency'=>$currencytype,'status'=>$status));
          
          $show       = $Page->show();// 分页显示输出
          // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
          $list = M('Orders')->where($where)->limit($Page->firstRow.','.$Page->listRows)->order('orders_id desc')->select();
          
          $this->assign('page',$show);// 赋值分页输出
          $this->assign('list',$list);
          $this->display();
     }
     
    //委托历史
    public function history(){
        $this->User_status();//判断是否需要进行信息补全
     //获取主币种
          $currency=$this->getCurrencyByCurrencyId();
          $this->assign('culist',$currency);
          
          $currencytype = I('currency');
          $status=I('status');
        if(!empty($currencytype)){
               $where['currency_id'] =$currencytype;
          }
          
          // $where['status'] = array('in','-1,2');
          $where['member_id'] = $_COOKIE['USER_KEY_ID'];
          $where['add_time'] = array('gt', strtotime(date('Y-m-d')));
          
     if(!empty($status) || $status === '0'){
               $where['status'] = $status;
          }
          // dump($where);die;
    $count      = M('Orders')->where($where)->count();// 查询满足要求的总记录数
          $Page       = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数
          
          //给分页传参数
          setPageParameter($Page, array('currency'=>$currencytype,'status'=>$status));
          
          $show       = $Page->show();// 分页显示输出
          // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
          $list = M('Orders')->where($where)->limit($Page->firstRow.','.$Page->listRows)->order('orders_id desc')->select();
          // echo M('orders')->getLastSql();die;
          
          $this->assign('page',$show);// 赋值分页输出
    
     $this->assign('list',$list);
          $this->display();
     }
    //委托历史
    public function lishi_history(){
        $this->User_status();//判断是否需要进行信息补全
     //获取主币种
          $currency=$this->getCurrencyByCurrencyId();
          $this->assign('culist',$currency);
          
          $currencytype = I('currency');
          $status=I('status');
        if(!empty($currencytype)){
               $where['currency_id'] =$currencytype;
          }
          
          // $where['status'] = array('in','-1,2');
          $where['member_id'] = $_COOKIE['USER_KEY_ID'];
          $where['add_time'] = array('lt', strtotime(date('Y-m-d')));
          
     if(!empty($status) || $status === '0'){
               $where['status'] = $status;
          }
          // dump($where);die;
    $count      = M('Orders')->where($where)->count();// 查询满足要求的总记录数
          $Page       = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数
          
          //给分页传参数
          setPageParameter($Page, array('currency'=>$currencytype,'status'=>$status));
          
          $show       = $Page->show();// 分页显示输出
          // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
          $list = M('Orders')->where($where)->limit($Page->firstRow.','.$Page->listRows)->order('orders_id desc')->select();
          // echo M('orders')->getLastSql();die;
          
          $this->assign('page',$show);// 赋值分页输出
    
     $this->assign('list',$list);
          $this->display();
     }
     
     /**
      *  撤销方法
      */
     public function cancel(){
               $order_id = I('post.order_id');              
               if(empty($order_id)){
                    $info['status'] = 0;
                    $info['info'] = '撤销订单不正确';
                    $this ->ajaxReturn($info);
               }
               //获取人的一个订单
               $one_order=$this->getOneOrdersByMemberAndOrderId($_COOKIE['USER_KEY_ID'], $order_id,array(0,1));
               if(empty($one_order)){
                    $info['status'] = -1;
                    $info['info'] = '传入信息错误';
                    $this ->ajaxReturn($info);
               }
               $info =   $this ->cancelOrdersByOrderId($one_order);
               $this ->ajaxReturn($info);
               
     }

     //买入矿机
     public function buy_mac(){
          $this->User_status();//判断是否需要进行信息补全

         $where['member_id']=$_COOKIE['USER_KEY_ID'];//用户id
               $count      = M("mac_orders")->where($where)->count();// 查询满足要求的总记录数
               $Page       = new \Think\Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数(25)
               $show       = $Page->show();// 分页显示输出// 进行分页数据查询 注意limit方法的参数要使用Page类的属性
               $list = M("mac_orders")->where($where)->order('mac_orders_id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
               $this->assign('list',$list);// 赋值数据集
               $this->assign('page',$show);// 赋值分页输出
          //星特币 昨天最后一笔成交价
          $time = strtotime(date('Y-m-d'));
          $newWhere['currency_id'] = 35;
          $newWhere['add_time'] = array('lt', $time);
          $price = M('trade')->where($newWhere)->order('trade_id desc')->getField('price');
          $big_buy = M('mac')->where(array('type'=>1))->getField('buy_money');
          $small_buy = M('mac')->where(array('type'=>2))->getField('buy_money');
          if($price){
               $this->assign('big_price', round($price*$big_buy, 2));
               $this->assign('small_price', round($price*$small_buy, 2));
          }else{
               $this->assign('big_price', $big_buy);
               $this->assign('small_price', $small_buy);
          }

          //矿机 到24号卖完
          if(time() > '1485187200'){
               $this->assign('stop', 1);
          }else{
               $this->assign('stop', 2);
          }

          //判断是否正在申请
          $judge = M('mac_orders')->where(array('member_id'=>cookie('USER_KEY_ID')))->order('mac_orders_id desc')->getField('status');
          if(in_array($judge, array('1', '2'))){
               $this->assign('theStatus', 2);
          }else{
               $this->assign('theStatus', 1);
          }
          
          $this->display();
     }

     public function ajax_deal_mac_big(){
               $num = I('post.num');
               $pwdtrade = I('post.pwdtrade');
               $type = I('post.type');
               $all_money = I('post.price');
               $info = M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->find();
               //检查数量
               if(!preg_match("/^[1-9][0-9]*$/", $num)){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'请填写正确的购买数量'));
               }
               //检查数量
               if($price < 0){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'请填写正确价格'));
               }
               // if($num > M('mac')->where(array('type'=>$type))->getField('remain_num')){
               //      $this->ajaxReturn(array('status'=>1, 'msg'=>'剩余矿机数量不足'));
               // }
               //检查密码
               if(md5($pwdtrade) != $info['pwdtrade']){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'交易密码不正确'));
               }
               //判断是否购买过 该类型矿机
               $theStatus = M('mac_orders')->where(array('member_id'=>cookie('USER_KEY_ID')))->order('mac_orders_id desc')->getField('status');
               if($theStatus && $theStatus == 1){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'您之前购买的矿机正在审核, 请耐心等待'));
               }
               if($theStatus && $theStatus == 2){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'您已购买过矿机, 矿机每人限购一台'));
               }
               // if($type == 1){
               //   //检查邀请码
               //   if(!M('member')->where(array('member_id'=>$agent, 'agent'=>1))->find()){
               //        $this->ajaxReturn(array('status'=>1, 'msg'=>'该用户不是代理中心'));
               //   }
               // }
               
               //检查金额
               $price = M('mac')->where(array('type'=>$type))->getField('buy_money');
               if($price*$num > $info['rmb']){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'账户金额不足'));
               }
               $mac_info = M('mac')->where(array('type'=>$type))->find();
               $end_time = time() + $mac_info['create_days']*24*3600;
               $data = array(
                         'mac_id'=>$mac_info['mac_id'],
                         'member_id'=>cookie('USER_KEY_ID'),
                         'num'=>$num,
                         'type'=>$type,
                         'add_time'=>time(),
                         'currency_id'=>$mac_info['currency_id'],
                         'end_time'=>$end_time,
                         'all_money'=>$all_money,
                         'status'=>1
                    );

               if(M('mac_orders')->add($data)){
                    //矿机表中数量 减少
                    M('mac')->where(array('type'=>$type))->setDec('remain_num', $num);
                    M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->setDec('rmb', $price*$num);
                    M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->setInc('forzen_rmb', $price*$num);
                    //该购买用户 如果购买了大矿机 需要后台进行审核
                    $this->ajaxReturn(array('status'=>2, 'msg'=>'您已购买成功, 请等待审核'));
               }else{
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'网络错误, 请稍后重试'));
               }
     }
     public function ajax_deal_mac_small(){
               $num = I('post.num');
               $pwdtrade = I('post.pwdtrade');
               $type = I('post.type');
               $all_money = I('post.price');
               $info = M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->find();
               //检查数量
               if($price < 0){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'请填写正确价格'));
               }
               //检查数量
               if(!preg_match("/^[1-9][0-9]*$/", $num)){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'请填写正确的购买数量'));
               }
               // if($num > M('mac')->where(array('type'=>$type))->getField('remain_num')){
               //      $this->ajaxReturn(array('status'=>1, 'msg'=>'剩余矿机数量不足'));
               // }
               //检查密码
               if(md5($pwdtrade) != $info['pwdtrade']){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'交易密码不正确'));
               }
               //判断是否购买过 该类型矿机
               $theStatus = M('mac_orders')->where(array('member_id'=>cookie('USER_KEY_ID')))->order('mac_orders_id desc')->getField('status');
               if($theStatus && $theStatus == 1){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'您之前购买的矿机正在审核, 请耐心等待'));
               }
               if($theStatus && $theStatus == 2){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'您已购买过矿机, 矿机每人限购一台'));
               }
               // if($type == 1){
               //   //检查邀请码
               //   if(!M('member')->where(array('member_id'=>$agent, 'agent'=>1))->find()){
               //        $this->ajaxReturn(array('status'=>1, 'msg'=>'该用户不是代理中心'));
               //   }
               // }
               
               //检查金额
               $price = M('mac')->where(array('type'=>$type))->getField('buy_money');
               if($price*$num > $info['rmb']){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'账户金额不足'));
               }
               $mac_info = M('mac')->where(array('type'=>$type))->find();
               $end_time = time() + $mac_info['create_days']*24*3600;
               $data = array(
                         'mac_id'=>$mac_info['mac_id'],
                         'member_id'=>cookie('USER_KEY_ID'),
                         'num'=>$num,
                         'type'=>$type,
                         'add_time'=>time(),
                         'currency_id'=>$mac_info['currency_id'],
                         'end_time'=>$end_time,
                         'all_money'=>$all_money,
                         'status'=>1
                    );

               if(M('mac_orders')->add($data)){
                    //矿机表中数量 减少
                    M('mac')->where(array('type'=>$type))->setDec('remain_num', $num);
                    M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->setDec('rmb', $price*$num);
                    M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->setInc('forzen_rmb', $price*$num);
                    //该购买用户 如果购买了大矿机 需要后台进行审核
                    $this->ajaxReturn(array('status'=>2, 'msg'=>'您已购买成功, 请等待审核'));
               }else{
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'网络错误, 请稍后重试'));
               }
     }


     /**
      * [apply description]
      * @return [type] 申请代理中心
      */
     public function apply(){
          $info = M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->find();
          $this->assign('info', $info);
          //判断是否正在申请
          if(M('apply')->where(array('member_id'=>cookie('USER_KEY_ID'), 'status'=>0))->find())
               $this->assign('theStatus', 1);
          else
               $this->assign('theStatus', 2);
          $this->display();
     }
     public function ajax_apply(){
               $name = I('post.name');
               $phone = I('post.phone');
               $remark = I('post.remark');
               $type = I('type');
               $info = M('member')->field('lower_agent,agent')->where(array('member_id'=>cookie('USER_KEY_ID')))->find();
               //如果申请的是 总代理
               if($type == 1){
                    //如果已经是 总代理
                    if($info['agent'] == 1){
                         $this->ajaxReturn(array('status'=>1, 'msg'=>'您已经是总代理中心，请不要重复申请'));
                    }
               }else{
               //如果申请的是 分代理
                    //如果已经是 分代理
                    if($info['lower_agent'] == 1){
                         $this->ajaxReturn(array('status'=>1, 'msg'=>'您已经是分代理中心，请不要重复申请'));
                    }
                    //如果已经是 总代理
                    if($info['agent'] == 1){
                         $this->ajaxReturn(array('status'=>1, 'msg'=>'您已经是总代理中心，不能再申请分代理中心'));
                    }
               }
               //判断是否 正在申请
               if(M('apply')->where(array('member_id'=>cookie('USER_KEY_ID')))->getField('status') === '0'){
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'您已提交过申请，请不要重复申请'));
               }
               $data = array(
                         'member_id'=>cookie('USER_KEY_ID'),
                         'name'=>$name,
                         'phone'=>$phone,
                         'remark'=>$remark,
                         'type'=>$type,
                         'status'=>0,
                         'add_time'=>time()
                    );
               if(M('apply')->add($data)){
                    $this->ajaxReturn(array('status'=>2, 'msg'=>'已提交申请，请耐心等待审核'));
               }else{
                    $this->ajaxReturn(array('status'=>1, 'msg'=>'网络错误，请稍后重试'));
               }
     }


     public function shouyilist(){
               $where['member_id'] = cookie('USER_KEY_ID');
               $where['status'] = 1;
               $count = M('invit')->where($where)->count();
               $sum_all = M('invit')->where($where)->sum('money');
               $this->assign('sum_all', $sum_all);
               $Page = new \Think\Page($count, 10);
               $show = $Page->show();
               $info = M('invit')->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('add_time asc')->select();
               foreach ($info as $key => $val) {
                    $val['lower_name'] = M('member')->where(array('member_id'=>$val['lower_id']))->getField('name');
                    $val['name'] = M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->getField('name');
                    $res[] = $val;
               }
               $this->assign('res', $res);
               $this->assign('page', $show);
               $this->display();
     }
     public function donglist(){
               $where['member_id'] = cookie('USER_KEY_ID');
               $where['status'] = 0;
               $count = M('invit')->where($where)->count();
               $sum_all = M('invit')->where($where)->sum('money');
               $this->assign('sum_all', $sum_all);
               $Page = new \Think\Page($count, 10);
               $show = $Page->show();
               $info = M('invit')->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('add_time asc')->select();
               foreach ($info as $key => $val) {
                    $val['lower_name'] = M('member')->where(array('member_id'=>$val['lower_id']))->getField('name');
                    $val['name'] = M('member')->where(array('member_id'=>cookie('USER_KEY_ID')))->getField('name');
                    $res[] = $val;
               }
               $this->assign('res', $res);
               $this->assign('page', $show);
               $this->display();
     }

     public function lower(){
               $pay = M('pay');$pay_log = M('pay_log');$trade = M('trade');$invit = M('invit');
               $where['pid'] = cookie('USER_KEY_ID');
               $count = M('member')->where($where)->count();
               $this->assign('sum_count', $count);
               $Page = new \Think\Page($count, 10);
               $show = $Page->show();
               $info = M('member')->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->select();
               foreach ($info as $key => $val) {
                    $val['per_chongzhi_all'] = $pay->where(array('member_id'=>$val['member_id'], 'status'=>1))->sum('money') + $pay_log->where(array('uid'=>$val['member_id'], 'status'=>2))->sum('money');
                    $val['per_trade_buy_kld'] = $trade->where(array('member_id'=>$val['member_id'], 'type'=>'buy', 'currency_id'=>'34'))->sum('money');
                    $val['per_trade_sell_kld'] = $trade->where(array('member_id'=>$val['member_id'], 'type'=>'sell', 'currency_id'=>'34'))->sum('money');
                    $val['per_trade_buy_xtb'] = $trade->where(array('member_id'=>$val['member_id'], 'type'=>'buy', 'currency_id'=>'35'))->sum('money');
                    $val['per_trade_sell_xtb'] = $trade->where(array('member_id'=>$val['member_id'], 'type'=>'sell', 'currency_id'=>'35'))->sum('money');
                    $val['per_get'] = $invit->where(array('lower_id'=>$val['member_id'], 'status'=>1))->sum('money');
                    $res[] = $val;
               }
               $this->assign('res', $res);
               $this->assign('page', $show);
               $this->display();
     }
     
}