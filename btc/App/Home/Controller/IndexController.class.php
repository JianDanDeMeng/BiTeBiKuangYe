<?php
namespace Home\Controller;

use Common\Controller\CommonController;

class IndexController extends CommonController
{
    public function _initialize()
    {
        parent::_initialize();
    }
    //空操作
    public function _empty()
    {
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }

    //刷新更新

    public function cnyjy()
    {

        //分类
        $type = M('currency_type')->where(array('status' => 1))->select();

        //币种
        foreach ($type as $key => $value) {
            $currency = M('currency')->where(array('type' => $value['id'], 'is_line' => 1))->order('sort')->select();
            //var_dump(M('currency')->getlastsql());
            foreach ($currency as $k => $v) {
                $list         = $this->getCurrencyMessageById($v['currency_id']);
                $currency[$k] = array_merge($list, $currency[$k]);
                $list['new_price'] ? $list['new_price'] : 0;
                $currency[$k]['currency_all_money'] = floatval($v['currency_all_num']) * $list['new_price'];
            }
            $type[$key]['currency'] = $currency;
        }

        $this->ajaxReturn($type[0]['currency']);
    }
    public function index()
    {
if ($this->isMobile()) header("Location: http://m.btcky.cc/"); 
        $article = M('article');
        //页面右方公告，提示，资信
        $art_model = D('Article');
        $info1     = $art_model->info(1);
        $info_red1 = $art_model->info_red(1); //标红
        $this->assign('info1', $info1); //非标红
        $this->assign('info_red1', $info_red1);
        $this->assign('isIndex', '1');
        $article_market = $article->where("position_id=2")->order('add_time desc')->limit(5)->select();
        $this->assign('article_market', $article_market);
        //获得媒体报道
        $article_media = $article->where("position_id=8")->order('add_time desc')->limit(5)->select();
        $this->assign('article_media', $article_media);
        //获得网站公告
        $article_web = $article->where("position_id=1")->order('add_time desc')->limit(5)->select();
        $this->assign('article_web', $article_web);

        //幻灯
        $flash = M('Flash')->order('sort')->limit(6)->select();
        $this->assign('flash', $flash);

        //币种
        $this->assign('currency_msg', M('currency')->where(['is_line'=>1, 'is_lock'=>0])->select());
        // $this->assign('currency_trend', $this->currency_trend());
        //*********选择进盟币,安全可信赖end*******
        $link      = M('Link');
        $link_info = $link->select();
        //截断友情链接url头，统一写法
        foreach ($link_info as $k => $v) {
            $url                  = "";
            $url                  = trim($v['url'], 'https://');
            $link_info[$k]['url'] = trim($url, 'http://');
        }
        $this->assign('link_info', $link_info);
        $this->assign('empty', '暂无数据');
        if (cookie('tips')) {
            $this->assign('is_tips', 1);
        } else {
            setcookie('tips', 1);
            $this->assign('is_tips', 2);
        }



		$all_money = M('Trade')->sum('money');
		//$all_money = $this->config['transaction_false']+$all_money;

		$all_money = (string)round($all_money);
		for($i=0;$i<strlen($all_money);$i++){
			$arr[strlen($all_money)-1-$i] = $all_money[$i];

		}
		$this->assign('arr',$arr);


        $sum_money = num_format($all_money);
        $this->assign('sum_money',$sum_money);
        $this->display();
    }
	
	
   public function isMobile(){  
 $useragent=isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';  
 $useragent_commentsblock=preg_match('|\(.*?\)|',$useragent,$matches)>0?$matches[0]:'';     
 function CheckSubstrs($substrs,$text){  
  foreach($substrs as $substr)  
   if(false!==strpos($text,$substr)){  
    return true;  
   }  
   return false;  
 }
 $mobile_os_list=array('Google Wireless Transcoder','Windows CE','WindowsCE','Symbian','Android','armv6l','armv5','Mobile','CentOS','mowser','AvantGo','Opera Mobi','J2ME/MIDP','Smartphone','Go.Web','Palm','iPAQ');
 $mobile_token_list=array('Profile/MIDP','Configuration/CLDC-','160×160','176×220','240×240','240×320','320×240','UP.Browser','UP.Link','SymbianOS','PalmOS','PocketPC','SonyEricsson','Nokia','BlackBerry','Vodafone','BenQ','Novarra-Vision','Iris','NetFront','HTC_','Xda_','SAMSUNG-SGH','Wapaka','DoCoMo','iPhone','iPod');  

 $found_mobile=CheckSubstrs($mobile_os_list,$useragent_commentsblock) ||  
     CheckSubstrs($mobile_token_list,$useragent);  

 if ($found_mobile){  
  return true;  
 }else{  
  return false;  
 }  
}



    /**
     * 最近7日价格趋势
     * @return [type] [description]
     */
    public function currency_trend(){
    	$currency = M('currency');
    	$trade = M('trade');
    	$currency_msg = $currency->where(['is_line'=>1, 'is_lock'=>0])->select();
    	$time_arr = $this->getLastSevenTime();
    	foreach($currency_msg as $key=>$val){
    		foreach($time_arr as $k=>$v){
    			$res[$val['currency_mark']][$k] = $trade->where("currency_id={$val['currency_id']} and add_time < $v")->order('add_time desc')->getField('price');
    		}
    	}
    	$this->ajaxReturn($res);
    }

    /**
     * 获得最近7日的 23:23:59的时间戳
     * @return [type] [description]
     */
    public function getLastSevenTime(){
    	for($i=6; $i>=0; $i--){
    		$arr[6-$i] = mktime(0,0,0,date('m'),date('d')-$i,date('Y'))-1;
    	}
    	return $arr;
    }

    /**
     * ajax首页数据刷新
     * @return [type] [description]
     */
    public function markets(){
    	$currency = M('currency');
    	$orders = M('orders');
    	$trade = M('trade');
    	$currency_info = $currency->field('currency_id,currency_name as display,currency_mark as coin_from')->where(['is_line'=>1, 'is_lock'=>0])->select();
    	foreach($currency_info as $key=>$val){
    		$arr['cny'][] = $this->getCurrencyMessageById($val['currency_id']);
    	}
    	$res['data'] = $arr;
    	$this->ajaxReturn($res);
    }

    /**
     * 获取当前币种的信息
     * @param int $id 币种id
     * @return 24H成交量 24H_done_num  24H成交额 24H_done_money 24H涨跌 24H_change 7D涨跌  7D_change
     * @return 最新价格 new_price 买一价 buy_one_price 卖一价 sell_one_price 最高价 max_price 最低价 min_price
     */

    public function getCurrencyMessageById($id)
    {
        $trade                = M('trade');
        $where['currency_id'] = $id;
        $time                 = time();
        //一天前的时间
        $old_time = strtotime(date('Y-m-d', $time));
        //最新价格
        $order             = 'add_time desc';
        $price1            = $trade->where($where)->order($order)->getField('price');
        $data['current'] = $price1;
        if($id == 35){
        	$data['total_value'] = $price1*21000000;
        }else{
        	$data['total_value'] = $price1*210000000;
        }
        $data['coin_from'] = M('currency')->where(['currency_id'=>$id])->getField('currency_mark');
        //判断价格是升是降
        $price2 = $trade->where($where)->where("add_time<$old_time")->order($order)->getField('price');
        if ($price2 > $price1) {
            //说明价格下降
            $data['new_price_status'] = 1;
        }else if($price2 == $price1){
        	$data['new_price_status'] = 2;
        } else {
            $data['new_price_status'] = 3;
        }
        $endYesterday = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
        $price3       = $trade->where($where)->where("add_time<$endYesterday")->order($order)->getField('price');
        if ($price3 != 0) {
            $data['24_ups'] = (float) sprintf("%.2f", ($price1 - $price3) * 100 / $price3 < 10 ?: 10);
            $data['24_ups'] = round(($price1 - $price3) * 100 / $price3, 2) < 10 ? round(($price1 - $price3) * 100 / $price3, 2) : 10;
        } else {
            $data['24_ups'] = 0;
        }
        $data['24_ups'] = $data['24_ups']/100;
        // //实时涨跌
        // $re = $trade->where($where)->where("add_time<$time")->order($order)->limit(9)->select();

        // $data['test'] = $re;

        //7D涨跌
        $price4 = $trade->where($where)->where("add_time<$time-60*60*24*7")->order($order)->getField('price');
        if ($price4 != 0) {
            $data['7_ups'] = sprintf("%.2f", ($price1 - $price4) / $price4 * 100);
            $data['7_ups'] = $data['7_ups']/100;
            if ($data['7_ups'] == 0) {
                $data['24H_change'] = 0;
            }
        } else {
            $data['24_ups'] = 0;
        }
        //24H成交量
        $rs                   = $trade->field('num')->where($where)->where("add_time>$time-60*60*24")->sum('num');
        $data['count'] = $rs ? $rs : 0;
        //24H成交额
        $rs                     = $trade->field('num*price')->where($where)->where("add_time>$time-60*60*24")->sum('num*price');
        $data['sum'] = $rs ? $rs : 0;
        $data['coin_to'] = 'cny';

        // //最低价
        // $data['min_price'] = $this->getminPriceTradeToday($id);
        // //最高价
        // $data['max_price'] = $this->getmaxPriceTradeToday($id);

        // //买一价
        // $data['buy_one_price'] = $this->getOneOrdersByPrice($id, 'buy');
        // //卖一价
        // $data['sell_one_price'] = $this->getOneOrdersByPrice($id, 'sell');
        //返回
        return $data;
    }

    public function zhilan(){
        $this->display();
    }
    public function chongbitibi(){
        $this->display();
    }
    public function jiaoyiguize(){
        $this->display();
    }
    public function yonghuxieyi(){
        $this->display();
    }
    public function guanyuwomen(){
        $this->display();
    }
}
