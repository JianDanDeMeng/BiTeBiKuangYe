<?php
/**
 *  更多参考 客户端rpc,获取完整支持方法列表
 */
namespace Home\Controller;

class RPCUtils{

  private function __construct(){}
  private function __clone(){}

  /**
   * sendtoaddress <jifenaddress> <amount> [comment] [comment-to]
   * @param unknown $url 钱包地址
   * @param unknown $money 提币数量
   * @return 交易的ID
   * 需要加密 *********************
   */
  public static function sendToAddress($url,$money,$currency){
    $bitcoin = self::getBitcoinInstance($currency);
    //$result = $bitcoin->getinfo();
    $bitcoin->walletlock();//强制上锁
    $bitcoin->walletpassphrase($currency['qianbao_key'],20);
    $id=$bitcoin->sendtoaddress($url,$money);
    $bitcoin->walletlock();
    return $id;
  }

  /**
   * 根据积分地址获取账户名称
   * getaccount <jifenaddress>
   * @param jifenaddress 积分地址
   * @return 账户名称
   */
  public static function getAccount($jifenaddress,$currency){
    $bitcoin=self::getBitcoinInstance($currency);
    return $bitcoin->getaccount($jifenaddress);
  }

  /**
   * 根据钱包账户获取账户地址
   * getaccountaddress <account>
   * @param $account
   * @return 地址
   */
  public static function getAccountAddress($account,$currency){
    $bitcoin=self::getBitcoinInstance($currency);
    return $bitcoin->getaccountaddress($account);
  }

  /**
   * 根据账户获取余额
   * getbalance [account] [minconf=1]
   * @param account
   * @return 账户余额
   */
  public static function getBalance($account,$currency){
    $bitcoin = self::getBitcoinInstance($currency);
    $bitcoin->walletlock();//强制上锁
    $bitcoin->walletpassphrase($currency['qianbao_key'],20);
    $balance=$bitcoin->getbalance($account);
    $bitcoin->walletlock();
    return $balance;
  }

  //获取钱包服务器资金总和
  public static function getTotalBalance($currency){
    $bitcoin = self::getBitcoinInstance($currency);
    $balance=$bitcoin->getbalance();
    return $balance;
  }

  /**
   * 根据交易ID获取相关交易记录
   * gettransaction <txid>
   * @param txid 交易ID
   * @return array
   */
  public static function getTransaction($txid,$currency){
    $bitcoin = self::getBitcoinInstance($currency);
    return $bitcoin->gettransaction($txid);
  }

//获取Bitcoin实例
  private static function getBitcoinInstance($currency){
    require_once 'App/Common/Common/easybitcoin.php';
    return new \Bitcoin($currency['rpc_user'],$currency['rpc_pwd'],$currency['rpc_url'],$currency['port_number']);
  }



}
 ?>
