<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 16-3-8
 * Time: 下午4:29
 */

namespace Home\Controller;

use Common\Controller\CommonController;

class ModifyMemberController extends CommonController {
	// public function __construct(){
	// 	if(!cookie('TMP_USER_KEY_ID')){
	// 		$this->redirect();
	// 	}
	// }
	/**
	 * 添加个人信息
	 */
	public function modify(){
		//判断是否是已经完成reg基本注册
		// $login=$this->checkLogin();
		// if(!$login){
		// 	$this->redirect('User/index');
		// 	return;
		// }
		// if(cookie('STATUS')!=0){
		// 	$this->redirect('User/index');
		// 	return;
		// }
		if(IS_POST){
			$M_member = D('Member');
			$id = cookie('TMP_USER_KEY_ID');
			$this->checkRegMsg('nick', I('post.nick'), '昵称已被占用');
			$this->checkRegMsg('name', I('post.name'), '姓名已被占用');
			$this->checkRegMsg('idcard', I('post.idcard'), '身份证号已被占用');
			//$this->checkRegMsg('phone', I('post.phone'), '手机号已被占用');
			$_POST['member_id']=$id;
			$_POST['status'] = 1;//0=有效但未填写个人信息1=有效并且填写完个人信息2=禁用
			if (!$data=$M_member->create()){ // 创建数据对象
				// 如果创建失败 表示验证没有通过 输出错误提示信息
				$data['status'] = 0;
				$data['info'] = $M_member->getError();
				$this->ajaxReturn($data);
				return;
			}else {
				$where['member_id'] = $id;
				$r = $M_member->where($where)->save();
				if($r){
					cookie('procedure',2);//SESSION 跟踪第二步
					// cookie('STATUS',1);
					$data['status'] = 1;
					$data['info'] = "提交成功";
					$this->ajaxReturn($data);
				}else{
					$data['status'] = 0;
					$data['info'] = '服务器繁忙，请稍后重试';
					$this->ajaxReturn($data);
				}
			}
		}else{
			$this->display();
		}
	}

	public function giveIdcard(){
		if(M('member_idcard')->where(['member_id'=>cookie('USER_KEY_ID'), 'status'=>1])->find()){
		    $this->redirect('User/index');return;
		}
		$member_id = cookie('USER_KEY_ID') ? cookie('USER_KEY_ID') : cookie('TMP_USER_KEY_ID');
		if(IS_POST){
			if(!$_FILES['idcard_zhen']['tmp_name'] || !$_FILES['idcard_fan']['tmp_name'] || !$_FILES['idcard_shou']['tmp_name']){
			    $this->ajaxReturn(['status'=>0, 'info'=>'请上传全部身份证照片']);
			}
			if($_FILES['idcard_zhen']['size'] > 300*1024 || $_FILES['idcard_fan']['size'] > 300*1024 || $_FILES['idcard_shou']['size'] > 300*1024){
			    $this->ajaxReturn(['status'=>0, 'info'=>'身份证照片不能超过 300kb']);
			}
			//存入身份证正反手持照片
			$s = $this->insertIdcardMsg($member_id, $_FILES);
			if($s){
				cookie('procedure',3);
				$this->ajaxReturn(['status'=>1, 'info'=>'提交成功']);
			}else{
				$this->ajaxReturn(['status'=>0, 'info'=>'服务器繁忙，请稍后重试']);
			}
		}
		$field = I('get.field');
		if($field == 'again'){
			M('member_idcard')->where(['member_id'=>$member_id])->delete();
		}
		$this->display();
	}
	/**
	 * 插入实名认证信息
	 * @return [type] [description]
	 */
	public function insertIdcardMsg($member_id, $file){
		$member_idcard = M('member_idcard');
		$arr['idcard_zhen'] = file_get_contents($file['idcard_zhen']['tmp_name']);
		$arr['idcard_zhen_type'] = $file['idcard_zhen']['type'];
		$arr['idcard_fan'] = file_get_contents($file['idcard_fan']['tmp_name']);
		$arr['idcard_fan_type'] = $file['idcard_fan']['type'];
		$arr['idcard_shou'] = file_get_contents($file['idcard_shou']['tmp_name']);
		$arr['idcard_shou_type'] = $file['idcard_shou']['type'];
		$arr['member_id'] = $member_id;
		$arr['status'] = 1;
		$arr['add_time'] = time();
		$res = $member_idcard->add($arr);
		if($res){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 验证注册信息
	 * @return [type] [description]
	 */
	public function checkRegMsg($field, $value, $msg){
		$member = M('member');
		$res = $member->where([$field=>$value])->find();
		if($res){
			$this->ajaxReturn(['status'=>0, 'info'=>$msg]);
		}
	}


	/**
	 *  ajax 验证身份证
	 */
	    public function ajaxCheckIdcard($idcard){
	        $idcard = urldecode($idcard);
	        $data = array();
	        $M_member = M('Member');
	        $where['idcard']  = $idcard;
	        $r = $M_member->where($where)->find();
	        if($r){
	            $data['msg'] = "此身份证已经绑定过!";
	            $data['status'] = 0;
	        }else{
	            $data['msg'] = "";
	            $data['status'] = 1;
	        }
	        $this->ajaxReturn($data);
	    }

	public function modify2(){
		//判断是否是已经完成reg基本注册
		$login=$this->checkLogin();
		if(!$login){
			$this->redirect('User/index');
			return;
		}
		if(cookie('STATUS')!=0){
			$this->redirect('User/index');
			return;
		}
		if(IS_POST){
			$M_member = D('Member');
			$id = cookie('USER_KEY_ID');
			$_POST['member_id']=$id;
			$_POST['status'] = 1;//0=有效但未填写个人信息1=有效并且填写完个人信息2=禁用
			if (!$data=$M_member->create()){ // 创建数据对象
				// 如果创建失败 表示验证没有通过 输出错误提示信息
				$data['status'] = 0;
				$data['info'] = $M_member->getError();
				$this->ajaxReturn($data);
					//$this->error($M_member->getError());
				return;
			}else {
				$where['member_id'] = $id;
				$r = $M_member->where($where)->save();
				if($r){
					cookie('procedure',2);//SESSION 跟踪第二步
					cookie('STATUS',1);
					$data['status'] = 1;
					$data['info'] = "提交成功";
					$this->ajaxReturn($data);
						//$this->redirect('Reg/regSuccess');
				}else{
					$data['status'] = 0;
					$data['info'] = '服务器繁忙,请稍后重试';
					$this->ajaxReturn($data);
						//$this->error('服务器繁忙,请稍后重试');
						//return;
				}
			}
		}else{
			$this->display();
		}
	}
	/**
	 * ajax验证昵称是否存在
	 */
	public function ajaxCheckNick($nick){
		$nick = urldecode($nick);
		$data =array();
		$M_member = M('Member');
		$where['nick']  = $nick;
		$r = $M_member->where($where)->find();
		if($r){
			$data['msg'] = "昵称已被占用";
			$data['status'] = 0;
		}else{
			$data['msg'] = "";
			$data['status'] = 1;
		}
		$this->ajaxReturn($data);
	}
	/**
	 * ajax手机验证
	 */
	function ajaxCheckPhone($phone) {
		$phone = urldecode($phone);
		$data = array();
		if(!checkMobile($phone)){
			$data['msg'] = "手机号不正确！";
			$data['status'] = 0;
		}else{
			$M_member = M('Member');
			$where['phone']  = $phone;
			$r = $M_member->where($where)->find();
			if($r){
				$data['msg'] = "此手机已经绑定过！请更换手机号";
				$data['status'] = 0;
			}else{
				$data['msg'] = "";
				$data['status'] = 1;
			}
		}
		$this->ajaxReturn($data);
	}

	/**
	 * ajax验证手机验证码
	 */
	public function ajaxSandPhone(){
		$phone = urldecode(I('phone'));
		if(empty($phone)){
			$data['status']=0;
			$data['info'] = "手机号码不能为空";
			$this->ajaxReturn($data);
		}
		if(!preg_match("/^1[34578]{1}\d{9}$/",$phone)){
			$data['status']=-1;
			$data['info'] = "手机号码不正确";
			$this->ajaxReturn($data);
		}
		$user_phone=M("Member")->field('phone')->where("phone='$phone'")->find();
		if (!empty($user_phone)){
			$data['status']=-2;
			$data['info'] = "手机号码已经存在";
			$this->ajaxReturn($data);
		}
		$r = sandPhone($phone,$this->config['CODE_NAME'],$this->config['CODE_USER_NAME'],$this->config['CODE_USER_PASS']);

		$name=chuanglan_status($r['1']);
		if($re[1]==0){
			$data['stauts']=1;
			$data['info']=$name;
			$this->ajaxReturn($data);exit;
		}else{
			$data['status'] =-2;
			$data['info']=$name;
			$this->ajaxReturn($data);exit;
		}

//			if($r!="短信发送成功"){
//				 $data['status']=0;
//				 $data['info'] = $r;
//				 $this->ajaxReturn($data);
//			}else{
//				 $data['status']=1;
//				 $data['info'] = $r;
//				 $this->ajaxReturn($data);
//			}
	}
}
