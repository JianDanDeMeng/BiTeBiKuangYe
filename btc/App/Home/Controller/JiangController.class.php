<?php
namespace Home\Controller;
use Common\Controller\CommonController;
class JiangController extends HomeController {
    public function _initialize(){
        parent::_initialize();

    }
    //空操作
    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
    public function choujiang(){
        $this->User_status();//判断是否需要进行信息补全
        $u_info = M('Member')->where("member_id = {$_COOKIE['USER_KEY_ID']}")->find();
        $this->assign('u_info',$u_info);
        $this->assign('empty','暂无数据');
        $this->display();
     }
    //判断是否在 注册后 7天内 充值 1000元
    public function panduan(){
        // 判断是否登录
        $login=$this->checkLogin();
        if(!$login){
            $msg['status'] = 1;
            $msg['info'] = '请先登录';
            $this->ajaxReturn($msg);
        }
        // $this->User_status();
        //判断是否在注册后七天后抽奖
        if(time()-$this->member['reg_time'] > 7*24*3600){
            $msg['status'] = 1;
            $msg['info'] = '很遗憾，抽奖于注册后7天后失效';
            $this->ajaxReturn($msg);
        }
        // $where['uid'] = $this->member['member_id'];
        // $where['addtime'] = array('between', array($this->member['reg_time'], $this->member['reg_time']+7*24*3600));
        // //查到注册后 7天内的 
        // $where['state'] = 2;
        // $where['money'] = array('egt', '1000');
        // $zhifubao = M('pay_log')->where($where)->find();

        // //人工充值金额
        // $newwhere['add_time'] = array('between', array($this->member['reg_time'], $this->member['reg_time']+7*24*3600));
        // $newwhere['status'] = 1;
        // $newwhere['member_id'] = $this->member['member_id'];
        // $newwhere['money'] = array('egt', '1000');
        // $rengong = M('pay')->where($newwhere)->find();
        $newwhere['add_time'] = array('between', array($this->member['reg_time'], $this->member['reg_time']+7*24*3600));
        $newwhere['member_id'] = $this->member['member_id'];
        $newwhere['type'] = 'buy';
        $trade_sum = M('trade')->where($newwhere)->sum('money');
        if($trade_sum < 1000){
            $msg['status'] = 1;
            $msg['info'] = '交易金额不足1000元，无法抽奖';
            $this->ajaxReturn($msg);
        }
        //查看是否抽奖过
        $temp['member_id'] = $this->member['member_id'];
        $temp['status'] = 1;
        $rs = M('choujiang')->where($temp)->select();
        if($rs){
            $msg['status'] = 1;
            $msg['info'] = '您已抽过奖了';
            $this->ajaxReturn($msg);
        }

        $msg['status'] = 2;
        $msg['info'] = '您很幸运,获得一次抽奖机会';
        $this->ajaxReturn($msg);
    }
    public function chongqian(){

        $money = intval($_POST['data']);
        if(empty($money)){
            return; 
        }

        //判断该用户 在currency_user 表中 有没有 赤币记录
        $where1['member_id'] = $this->member['member_id'];
        $where1['currency_id'] = 35;
        $M_user = M('currency_user')->where($where1)->find();
        //如果有记录  则判断 钱包地址是否存在
        if(!$M_user['chongzhi_url']){
            //如果钱包地址不存在
            $id = 35;
            require_once 'App/Common/Common/easybitcoin.php';           
            $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
            $re = $_COOKIE['USER_KEY_ID'].$id;
            //得到 该用户赤币地址
            $address = $bitcoin->getaccountaddress($re);
            //将赤币地址赋给  该用户在 currency_user 的 chongzhi_url 字段
            M('currency_user')->where($where1)->setField('chongzhi_url', $address);
            //此时该用户赤币地址存在
        }
        //给该用户 currency_user 中赤币 加上 抽奖所得
        $this->setUserMoney($_COOKIE['USER_KEY_ID'], 35, $money, 'inc', 'num');    
        $this->addMessage_all($this->member['member_id'], -2, '恭喜您抽奖获得星特盾', '抽奖获得'.$money.'枚星特盾，已发放');
        //向抽奖日志表中插入一条记录
        $data2['member_id'] = $this->member['member_id'];
        $data2['type'] = 35;
        $data2['money'] = $money;
        $data2['status'] = 1;
        $data2['add_time'] = time();
        M('choujiang')->add($data2);
        /*
            向钱包充钱
         */
        require_once 'App/Common/Common/easybitcoin.php'; 
        $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','47.93.116.40','8082');
        $a = $this->member['member_id'].'35';
        $c = floatval($money);
        $bitcoin->move('8888', $a, $c);
        $msg['status'] = 1;
        $this->ajaxReturn($msg);
    }
    // 抽奖分5个奖项
    // 30元数字货币
    // 50元数字货币
    // 100元数字货币
    // 200元数字货币
    // 300元数字货币

    // 《健》锋所指 2016/10/14 17:17:27
    // 中奖几率设置
    // 300，每天两个
    // 200，每天两个
    // 100，每天5个
    // 50，每天10个
    // 其余的都中30的
    
    public function suanfa(){
        //先查看一天之内的抽奖记录
        //当天0点 的时间
        $dayBegin = strtotime('Y-m-d', time());
        $where['addtime'] = array('between', array($dayBegin, time()));
            //找到当天时间内  抽奖金额为 888 的记录数量
        $where['money'] = 888;
        $count888 = M('choujiang')->where($where)->count();
            //找到当天时间内  抽奖金额为 388 的记录数量
        $where['money'] = 388;
        $count388 = M('choujiang')->where($where)->count();
            //找到当天时间内  抽奖金额为 188 的记录数量
        $where['money'] = 188;
        $count188 = M('choujiang')->where($where)->count();
            //找到当天时间内  抽奖金额为 88 的记录数量
        $where['money'] = 88;
        $count88 = M('choujiang')->where($where)->count();
            //找到当天时间内  抽奖金额为 8 的记录数量
        $where['money'] = 8;
        $count8 = M('choujiang')->where($where)->count();
        //如果 抽奖金额为 888 的记录数量 大于 2
        if($count888){

        }
    }
    //产生一个 随机数
    public function suiji($num){
        $any = random(1, 100);
        //当可以 产生 5种奖时
        if($num == 5){
            if($any <= 5){
                return 1;
            }else if($any > 5 && $any <= 15){
                return 2;
            }else if($any > 15 && $any <= 30){
                return 3;
            }else if($any > 30 && $any <= 50){
                return 4;
            }else if($any > 50 && $any <= 100){
                return 5;
            }
        }
        //当可以 产生 4种奖时
        if($num == 4){
            if($any <= 5){
                return 1;
            }else if($any > 5 && $any <= 15){
                return 2;
            }else if($any > 15 && $any <= 30){
                return 3;
            }else if($any > 30 && $any <= 100){
                return 4;
            }
        }
        //当可以 产生 3种奖时
        if($num == 3){
            if($any <= 5){
                return 1;
            }else if($any > 5 && $any <= 15){
                return 2;
            }else if($any > 15 && $any <= 100){
                return 3;
            }
        }
        //当可以 产生 2种奖时
        if($num == 2){
            if($any <= 5){
                return 1;
            }else if($any > 5 && $any <= 100){
                return 2;
            }
        }
        //当可以 产生 1种奖时
        if($num == 1){
            return 1;
        }
    }
}