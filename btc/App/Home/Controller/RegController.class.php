<?php
namespace Home\Controller;

use Common\Controller\CommonController;
class RegController extends CommonController {

    /**
     * 显示注册界面
     */
    public function reg(){
        if(cookie('USER_KEY_ID')){
            $this->redirect('User/index');
            return;
        }
        $pid = I('get.Member_id','','intval');
        $this->assign('pid',$pid);
        $this->display();
    }

     public function regTest(){
        if(cookie('USER_KEY_ID')){
            $this->redirect('User/index');
            return;
        }
        $pid = I('get.Member_id','','intval');
        $this->assign('pid',$pid);
        $this->display();
    }
    /**
     * 显示服务条款
     */
    public function terms(){
//         $list = M('Article')->where(array('article_id'=>125))->find();
//         $this->assign('list',$list);
        $this->display();
    }
    /**
     * 添加注册用户
     */
    public function addReg(){
        if(IS_POST){
            //增加添加时间,IP
            $_POST['reg_time'] = time();
            $_POST['ip'] = get_client_ip();
            $M_member = D('Member');
            if($_POST['pwd']==$_POST['pwdtrade']){
            	$data['status'] = 0;
            	$data['info'] = "交易密码不能和密码一样";
            	$this->ajaxReturn($data);
            }
            if (!$M_member->create()){
                // 如果创建失败 表示验证没有通过 输出错误提示信息
                $data['status'] = 0;
                $data['info'] = $M_member->getError();
                $this->ajaxReturn($data);
            }else{
                $r = $M_member->add();
                if($r){
                    cookie('TMP_USER_KEY_ID',$r);//传入cookie避免直接进入个人信息界面
                    // cookie('USER_KEY',$_POST['email']);//用户名
                    cookie('STAUTS',0);
                    cookie('procedure',1);//SESSION 跟踪第一步
                    $data['status'] = 1;
                    $data['info'] = '提交成功';
                    $this->ajaxReturn($data);
                }else{
                    $data['status'] = 0;
                    $data['info'] = '服务器繁忙,请稍后重试';
                    $this->ajaxReturn($data);
                }
            }
        }else{
            $this->display('Reg/reg');
        }
    }

    /**
     * 注册成功
     */
    public function regSuccess(){
        if(cookie('procedure') == 1){
            $this->redirect('ModifyMember/modify');
            return;
        }
        //判断步骤并重置
        if(cookie('procedure') == 2){
            $this->redirect('ModifyMember/giveIdcard');
            return;
        }
        if(cookie('procedure') == 3){
            cookie('procedure',null);
            $this->display();
        }

    }

    /**
     * ajax验证邮箱
     * @param string $email 规定传参数的结构
     *
     */
    public function ajaxCheckEmail($email){
        $email = urldecode($email);
        $data = array();

        $M_member = M('Member');
        $where['phone']  = $email;
        $r = $M_member->where($where)->find();
        if($r){
            $data['status'] = 0;
            $data['msg'] = "手机号已存在";
        }else{
            $data['status'] = 1;
            $data['msg'] = "";
        }

        $this->ajaxReturn($data);
    }

    public function ajaxyzm_code($email,$pid){
	
	 	$M_member = M('Member');
        $where['member_id']  = $pid;
        $r = $M_member->where($where)->find();
		if(!$r){
		 $data['status'] = 0;
            $data['msg'] = "邀请人不存在";
			 $this->ajaxReturn($data);
		}
		
        $email = urldecode($email);
        $map['member_id'] = 0;
        $map['member_ip'] = get_client_ip();
        //   $mapp['add_time'] =  array(between,array('1','8'));
        $yanzhengma =  M('yanzhengma')->where($map)->order('add_time desc')->find();
        if($yanzhengma['code']!=$email or $email==''){
            $data['status'] = 0;
            $data['msg'] = "验证码错误";
        }else{
            $data['status'] = 1;
            $data['msg'] = "成功";
        }

        $this->ajaxReturn($data);
    }

    public function up_phone(){
        $code = $_POST['code'];
        $phone = $_POST['phone'];
        $pwd = $_POST['pwd'];
        $map['member_id'] = 0;
        $map['member_ip'] = get_client_ip();
        //   $mapp['add_time'] =  array(between,array('1','8'));
        $yanzhengma =  M('yanzhengma')->where($map)->order('add_time desc')->find();
        if($yanzhengma['code']!=$code){
            $data['status'] = 0;
            $data['msg'] = "验证码错误";
            $this->ajaxReturn($data);
        }

        $member = M('member')->where(array('phone'=>$phone))->find();

        if(!$member){
            $data['status'] = 0;
            $data['msg'] = "手机号不存在";
            $this->ajaxReturn($data);
        }
        M('member')->where(array('phone'=>$phone))->setField('pwd',md5($pwd));
        $data['status'] = 1;
        $data['msg'] = "修改成功";
        $this->ajaxReturn($data);



    }

}
