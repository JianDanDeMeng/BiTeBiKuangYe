<?php
namespace Home\Controller;
use Home\Controller\HomeController;
use Think\Page;
use Think\Upload;

class PayController extends HomeController {

    //空操作
    public function _initialize(){
        parent::_initialize();
    }

    public function _empty(){
        header("HTTP/1.0 404 Not Found");
        $this->display('Public:404');
    }
    public function index(){
        $this->display();
    }
    //人工充值AJAX处理方式
    public function rechargeByMan(){
        $config=$this->config;
        $member=$this->member;
        $data['member_name']=I('post.member_name');
        $data['money']=intval(I('post.money'));
        $data['account']=I('post.account');
        $data['count']=I('post.count');
        $data['type']=I('post.type');
        $data['remark']=I('post.remark');

        $theCode = M('yanzhengma')->where(array('member_id'=>cookie('USER_KEY_ID'),'code'=>$_POST['code'], 'status'=>1))->getField('code');
        if($_POST['code'] != $theCode){
            $arr['info']='短信验证错误';
            $arr['status']=0;
            $this->ajaxReturn($arr);
        }
        M('yanzhengma')->where(array('member_id'=>cookie('USER_KEY_ID'), 'status'=>1))->setField('status', 2);


        if(empty($data['member_name'])||empty($data['money'])||empty($data['account'])||empty($data['type'])){
            $arr['info']='请填写全部信息';
            $arr['status']=0;
            $this->ajaxReturn($arr);
        }
        if($member['status']!=1){
            $arr['info']='请完成实名验证再进行充值';
            $arr['status']=0;
            $this->ajaxReturn($arr);
        }
        if(strlen($data['account'])<11||strlen($data['account'])>20){
            $arr['info']='请输入正确的银行卡号或支付宝账号';
            $arr['status']=0;
            $this->ajaxReturn($arr);
        }
        if($data['money']<$config['pay_min_money']){
            $arr['info']="充值金额不能小于{$config['pay_min_money']}元";
            $arr['status']=0;
            $this->ajaxReturn($arr);
        }
        if($data['money']>$config['pay_max_money']){
            $arr['info']="充值金额不能大于{$config['pay_max_money']}元";
            $arr['status']=0;
            $this->ajaxReturn($arr);
        }
        $data['member_id'] = cookie('USER_KEY_ID');
        $data['add_time']=time();
        $data['status']=0;
        $list=M('Pay')->add($data);
        if($list){
            $arr['info']="充值订单提交成功";
            $arr['status']=1;
            $arr['num']=$data['count'];
            $this->ajaxReturn($arr);
        }else{
            $arr['info']="充值订单提交失败";
            $arr['status']=0;
            $this->ajaxReturn($arr);
        }
    }

    //币充值页面
    public function bpay(){

        $this->User_status();//判断是否需要进行信息补全
        $id=I('currency_id');//货币id
        $this->check_address($_COOKIE['USER_KEY_ID'], $id);
        if(empty($id)){
            $this->error("请选择币种",U("User/index"));
        }
        $currency=$this->getCurrencyByCurrencyId($id);

        if(empty($currency)){
            $this->error("无效币种请联系管理员",U("User/index"));
        }
        $list=$this->getUserMoneyByCurrencyId($_COOKIE['USER_KEY_ID'], $id);
		
		
        //设置充值地址
        if(empty($list['chongzhi_url']) || strpos($list['chongzhi_url'], 'connect')){

            require_once 'App/Common/Common/easybitcoin.php';
           
			
			$currency = M('currency')->where(array('currency_id'=>$id))->find();
          $bitcoin = new \Bitcoin($currency['rpc_user'],$currency['rpc_pwd'],$currency['rpc_url'],$currency['port_number']);
			
            $re = $_COOKIE['USER_KEY_ID'].$id;
            $address = $bitcoin->getaccountaddress($re);

            $this->setCurrentyMemberByMemberId($_COOKIE['USER_KEY_ID'], $id, 'chongzhi_url', $address);
            $list['chongzhi_url']=$address;
        }

        //充值页面
//        $where['u_id']=$_COOKIE['USER_KEY_ID'];
//        $where['c_cid']=$id;//货币id
//        import('ORG.Util.Page');// 导入分页类
//        $count      = M("pay_zhuanrqb")->where($where)->count();// 查询满足要求的总记录数
//        $Page       = new Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数
//        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
//        $chongzhi = M("pay_zhuanrqb")->where($where)->order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
//        $this->assign('chongzhi',$chongzhi);// 赋值数据集
//        $this->assign('page',$show);// 赋值分页输出
        $this->assign("list",$list);
        $this->assign("currency",$currency);//货币信息

        $save_path = ROOT_PATH.'Public/qrcode/';  //图片存储的绝对路径
        $web_path = '/ROOT_PATHPublic/qrcode/';        //图片在网页上显示的路径
        $qr_data = 'RC-URL-'.$list['chongzhi_url'];
        $qr_level = 'H';
        $qr_size = '4';
        $save_prefix ='ZETA';
        if($filename = $this->createQRcode($save_path,$qr_data,$qr_level,$qr_size,$save_prefix)){
            $pic = $web_path.$filename;
        }

        $this->assign("pic",$pic);
        $this->display();
    }


    //提币的页面
    public function tcoin(){

        $this->User_status();//判断是否需要进行信息补全
        $cuid=I('currency_id');
        $list=M("Qianbao_address")->where("user_id='{$_COOKIE['USER_KEY_ID']}' and currency_id = '$cuid'")->order('id desc')->select();
        $this->assign("list",$list);
        $currency=$this->getCurrencyByCurrencyId($cuid);
        if(empty($currency)){
            $this->error("错误操作，请联系管理员",U('User/index'));
        }

        $where['u_id']=$_COOKIE['USER_KEY_ID'];
        $where['c_cid']=$cuid;
        import('ORG.Util.Page');// 导入分页类
        $count      = M("pay_tibiqb")->where($where)->count();// 查询满足要求的总记录数
        $Page       = new Page($count,10);// 实例化分页类 传入总记录数和每页显示的记录数
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $tibi_list = M("pay_tibiqb")->where($where)->order("id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign('page',$show);// 赋值分页输出
        $this->assign("tibi_list",$tibi_list);
        $this->tibi_save($currency);

        $cuser=M("Currency_user")->where("currency_id='$cuid' and member_id='{$_COOKIE['USER_KEY_ID']}'")->find();
        $this->assign("cuser",$cuser);//个人货币数量

        $this->assign("cuid",$currency);//货币名称

        $list = M('pay_tibisxf')->where('id=1')->find();
        if($cuid==26){
        $this->assign("tcoin_fee",$list['laite']+'0.001');
        }
        if($cuid==34){
        $this->assign("tcoin_fee",$list['chibi']+'0.01');
        }
        if($cuid==35){
        $this->assign("tcoin_fee",$list['xtb']+'0.001');
        }
        if($cuid==28){
        $this->assign("tcoin_fee",$list['bite']+'0.0001');
        }
        // $idcard_info = $this->getIdcardCheck();
        // $this->assign('idcard_info_status', $idcard_info['status']);
        // $this->assign('idcard_info_msg', $idcard_info['msg']);
        $this->display();
    }


    //转入币程序 xwtb

    public function ordras(){
        if($_COOKIE['USER_KEY_ID']==''){
             exit;
           }


        $cid = $_POST['cid'];
        require_once 'App/Common/Common/easybitcoin.php';

       
		
			$currency = M('currency')->where(array('currency_id'=>$cid))->find();
          $bitcoin = new \Bitcoin($currency['rpc_user'],$currency['rpc_pwd'],$currency['rpc_url'],$currency['port_number']);

        $list = $bitcoin->listtransactions($_COOKIE['USER_KEY_ID'].$cid, 999, 0);
        $new_time = 1488124800;
        foreach($list as $k=>$v){
            if($v['time']>$new_time){
               if($v['category']=='receive'){
                  $map['blockhash'] = $v['blockhash'];
                  $map['u_id'] = $_COOKIE['USER_KEY_ID'];
                  $map['txid'] = $v['txid'];
                  $hash = M('pay_zhuanrqb')->where($map)->find();
                   if(!$hash){
                     $zhub['num'] = $v['amount'];
                     $zhub['u_id'] = $_COOKIE['USER_KEY_ID'];
                     $zhub['u_dizhi'] = $v['address'];
                     $zhub['c_cid'] = $cid;
                     $zhub['c_ip'] = $_SERVER["REMOTE_ADDR"];
                     $zhub['ctime'] = time();
                     $zhub['account'] = $v['account'];
                     $zhub['category'] = $v['category'];
                     $zhub['amount'] = $v['amount'];
                     $zhub['time'] = $v['time'];
                     $zhub['blockhash'] = $v['blockhash'];
                     $zhub['txid'] = $v['txid'];
                     $addpay = M('pay_zhuanrqb')->add($zhub);
           if($addpay){
           $cumap['member_id'] = $_COOKIE['USER_KEY_ID'];
           $cumap['currency_id'] = $cid;
           $cumap['chongzhi_url'] = $v['address'];
           M('currency_user')->where($cumap)->setInc('num',$v['amount']);
           }
           $v['amount'] = '';
                  }
               }
            }
        }
    }


//    //提币程序 xwtb
//
//    public function tcoinsa(){
//     // $this->error('提币功能正在升级维护！');exit;
//    $sxf = M('pay_tibisxf')->where('id=1')->find();
//    $cuid = $_POST['currency_id'];
//    if (!preg_match('/^[0-9]+(.[0-9]{1,4})?$/', floatval($_POST['num']))) {
//             $this->error('非法输入,小数点后只能留4位');exit;
//        }
//
//
//
//    if($_POST['currency_id']==26){
//
//     $zmus = floatval($_POST['num'])+($sxf['laite']+'0.001');
//
//    }
//    if($_POST['currency_id']==35){
//
//     $zmus = floatval($_POST['num'])+($sxf['xtb']+'0.001');
//
//    }
//    if($_POST['currency_id']==34){
//
//    $zmus = floatval($_POST['num'])+($sxf['chibi']+'0.01');
//    }
//    if($_POST['currency_id']==28){
//
//      $zmus = floatval($_POST['num'])+($sxf['bite']+'0.0001');
//    }
//
//    $post_dizhi = I('dizhi');
//    if(empty($post_dizhi)){
//      $this->error('请选择提币的地址');exit;
//    }
//
//   $list=M("Qianbao_address")->where(['id'=>$post_dizhi])->find();
//
//   if($list['qianbao_url']==''){
//   $this->error('钱包地址没有设置！');exit;
//   }
//   if(floatval($_POST["num"])=='' or floatval($_POST["num"])<0.0001){
//    $this->error('数量非法！');exit;
//    }
//
//
//   $dizhi = preg_replace("/\s|　/","",$list["qianbao_url"]);
//
//    if($_POST['code']==''){
//     $this->error('短信不可以为空');exit;
//    }..
//
//    $tb['currency_id'] = $_POST['currency_id'];
//    $tb['member_id'] = $_COOKIE['USER_KEY_ID'];
//    $tb['num'] = array('egt',floatval($zmus));
//    $tbs = M('currency_user')->where($tb)->find();
//
//    if(!$tbs){
//    $this->error('数量超出了最大额！');exit;
//    }
//
//
//        require_once 'App/Common/Common/assaeasybitcoin.php';
//
//        $uid= $_COOKIE['USER_KEY_ID'].$_POST['currency_id'];
//
//        //莱特币
//        if($_POST['currency_id']==26){
//            $bitcoin = new \Bitcoin('user','P2kAlMiG3Kb8FzP','114.215.44.55','9148');
//        $tbnum = $bitcoin->getbalance($uid);
//     // if(abs($tbnum-floatval($zmus)) > 2){
//     //    $this->error('刚购买的币种区块钱包同步中,请稍后再提币');exit;
//     //   }
//
//        $diz = $bitcoin->validateaddress($dizhi);
//        if($diz['isvalid']==false){
//         $this->error('钱包地址错误');exit;
//        }
//
//            $ids= $bitcoin->sendfrom($_COOKIE['USER_KEY_ID'].$_POST['currency_id'],$dizhi,floatval($_POST["num"]));//$_POST["num"]  这个函数 转数字型floatval
//            if($sxf['laite']>0){
//                      $sxflog = $bitcoin->move($uid,'',floatval($sxf['laite']));
//                         $str= date('Y-m-d H:i:s').'>>提ID--'.$uid."--提现手续费￥ ".$sxf['laite']." ￥返回码--".$sxflog."\r\n";
//                        $file_pointer = fopen("./jylog/".date('Y-m-d').'sxf.'.txt,"a+");
//                        fwrite($file_pointer,$str);
//          }
//
//        }
//
//
//        // var_dump($_POST["sum"]);exit;
//         $str= '用户ID--'.$_COOKIE['USER_KEY_ID'].'--发送数量--'.floatval($_POST["num"]).'--地址'.$dizhi.'--币种--'.$_POST['currency_id'].'状态码》》'.$ids."\r\n";
//        $file_pointer = fopen("./jylog/".date('Y-m-d').'tibi.'.txt,"a+");
//        fwrite($file_pointer,$str);
//
//
//        if($ids!=false){
//
//        $vos['num'] = floatval($_POST["num"]);
//        $vos['u_id'] = $_COOKIE['USER_KEY_ID'];
//        $vos['u_dizhi'] = $dizhi;
//        $vos['c_cid'] = $_POST['currency_id'];
//        $vos['c_ip'] = $_SERVER["REMOTE_ADDR"];
//        $vos['ctime'] = time();
//        M('pay_tibiqb')->add($vos);
//
//        $tba['currency_id'] = $_POST['currency_id'];
//        $tba['member_id'] = $_COOKIE['USER_KEY_ID'];
//        $tbsa = M('currency_user')->where($tba)->setDec('num',$zmus);
//
//        if($tbsa){
//
//        $this->success('提币成功');
//        }
//
//        }else{
//
//        $this->error('异常错误');
//        }
//
//    }


    //提币程序 xwtb

    public function tcoinsa(){
        // $this->error('提币功能正在升级维护！');exit;
        $sxf = M('pay_tibisxf')->where('id=1')->find();
        $cuid = $_POST['currency_id'];
        if (!preg_match('/^[0-9]+(.[0-9]{1,4})?$/', floatval($_POST['num']))) {
            $this->error('非法输入,小数点后只能留4位');exit;
        }
        if($_POST['currency_id']==26){
            $zmus = floatval($_POST['num'])+($sxf['laite']+'0.001');
        }
        $post_dizhi = I('dizhi');
        if(empty($post_dizhi)){
            $this->error('请选择提币的地址');exit;
        }
        $list=M("Qianbao_address")->where(['id'=>$post_dizhi])->find();
        if($list['qianbao_url']==''){
            $this->error('钱包地址没有设置！');exit;
        }
        if(floatval($_POST["num"])=='' or floatval($_POST["num"])<0.0001){
            $this->error('数量非法！');exit;
        }
        $dizhi = preg_replace("/\s|　/","",$list["qianbao_url"]);
        if($_POST['code']==''){
            $this->error('短信不可以为空');exit;
        }
        $theCode = M('yanzhengma')->where(array('member_id'=>cookie('USER_KEY_ID'),'code'=>$_POST['code'], 'status'=>1))->getField('code');
        if($_POST['code'] != $theCode){
            $this->error('短信验证错误');exit;
        }
        M('yanzhengma')->where(array('member_id'=>cookie('USER_KEY_ID'), 'status'=>1))->setField('status', 2);
        $_COOKIE['code'] = '';
        $map['member_id'] = $_COOKIE['USER_KEY_ID'];
        //  $map['pwdtrade'] = md5($_POST['paypwd']);
        $paw = M('member')->where($map)->find();
        if($paw['pwdtrade']!=md5($_POST['paypwd'])){
            $this->error('支付密码错误！');exit;
        }
        $tb['currency_id'] = $_POST['currency_id'];
        $tb['member_id'] = $_COOKIE['USER_KEY_ID'];
        $tb['num'] = array('egt',floatval($zmus));
        $tbs = M('currency_user')->where($tb)->find();
        if(!$tbs){
            $this->error('数量超出了最大额！');exit;
        }
        $vos['num'] = floatval($_POST["num"]);
        $vos['u_id'] = $_COOKIE['USER_KEY_ID'];
        $vos['u_dizhi'] = $dizhi;
        $vos['c_cid'] = $_POST['currency_id'];
        $vos['c_ip'] = $_SERVER["REMOTE_ADDR"];
        $vos['ctime'] = time();
        M('pay_tibiqb')->add($vos);
        $tba['currency_id'] = $_POST['currency_id'];
        $tba['member_id'] = $_COOKIE['USER_KEY_ID'];
        $tbsa = M('currency_user')->where($tba)->setDec('num',$zmus);
//        $forzen_desc = M('currency_user')->where($tba)->setInc('forzen_num',$zmus);
        if($tbsa){
            $this->success('提币成功');
        }else{
            $this->error('提币失败');
        }
    }


    /**
     * 修改提币记录
     */
    private function tibi_save($currency){
        $where['user_id']=$_COOKIE['USER_KEY_ID'];
        $where['status']=0;
        $list=M("Tibi")->where($where)->select();
        if(!empty($list)){
            foreach ($list as $k=>$v){
                $x="";
                $data="";
                $x=$this->chakan_tibi_jilu($v['ti_id'],$currency);
                if($x['confirmations']>2){
                    $data['check_time']=$x['time'];
                    $data['status']=1;
                    M("Tibi")->where("id='{$v['id']}'")->save($data);
                }
            }
        }
    }
    /**
     *
     * 查看提币记录
     *
     * @param unknown $tid 提币ti_id
     * @return unknown
     */
    private function chakan_tibi_jilu($tid,$currency){
        require_once 'App/Common/Common/easybitcoin.php';
        $bitcoin = new \Bitcoin($currency['rpc_user'],$currency['rpc_pwd'],$currency['rpc_url'],$currency['port_number']);
//      $result = $bitcoin->getinfo();
        $list= $bitcoin->gettransaction($tid);
        return $list;
    }

    /**
     * 提币方法
     */

    public function ti_bi(){
         $cuid=I("currency_id");//货币id
         $currency=M("Currency")->where("currency_id='$cuid'")->find();//这个是货币
         if($_POST['code']!=$_COOKIE['code']){
            $arr['status']=11;
            $arr['info']="验证码不正确";
            $this->ajaxReturn($arr);exit;
         }
         if(empty($currency)){
            $arr['status']=10;
            $arr['info']="无效货币，无法提币";
            $this->ajaxReturn($arr);exit;
         }

        if(empty($_POST['num'])){
            $arr['status']=2;
            $arr['info']="请填写提币数量";
            $this->ajaxReturn($arr);exit;
        }
        if($_POST['num']<=0.0001){
            $arr['status']=3;
            $arr['info']="请填写大于0.0001的数量";
            $this->ajaxReturn($arr);exit;
        }
        if($_POST['num']>$currency['currency_all_tibi']){
            $arr['status']=4;
            $arr['info']="已超出最大限制";
            $this->ajaxReturn($arr);exit;
        }
        $num=floatval($_POST['num']);
        if(empty($_POST['paypwd'])){
            $arr['status']=5;
            $arr['info']="请填写支付密码";
            $this->ajaxReturn($arr);exit;
        }
        $user=M("Member")->where("member_id='{$_COOKIE['USER_KEY_ID']}'")->find();
        if($user['pwdtrade']!=md5($_POST['paypwd'])){
            $arr['status']=6;
            $arr['info']="支付密码错误";
            $this->ajaxReturn($arr);exit;
        }

            //判断是否已经添加提币地址
        $list=M("Qianbao_address")->where("user_id='{$_COOKIE['USER_KEY_ID']}' and currency_id='$cuid'")->find();

        if(empty($list)){
            $arr['status']=7;
            $arr['info']="请添加提币地址";
            $this->ajaxReturn($arr);exit;
        }
        //判断看这个钱包地址是否是真实地址
        if(!$this->check_qianbao_address($list['qianbao_url'],$currency)){
            $arr['status']=8;
            $arr['info']="提币地址不是一个有效地址";
            $this->ajaxReturn($arr);exit;
        }
        //判断账户余额够不够
        $money=M("Currency_user")->where("member_id='{$_COOKIE['USER_KEY_ID']}' and currency_id='$cuid'")->find();
        if($money['num']<$num){
            $arr['status']=10;
            $arr['info']="账户余额不足，无法提币";
            $this->ajaxReturn($arr);exit;
        }
        if(!empty($this->config['tcoin_fee'])){
            $actual=$num*(1-$this->config['tcoin_fee']/100);//计算出扣除手续费后的价格
        }else{
            $actual=$num;
        }
        $actual=(float)$actual;//实际到账

        $data['fee']=$this->config['tcoin_fee'];//手续费
        $data['currency_id']=$cuid;
        $data['user_id']=$_COOKIE['USER_KEY_ID'];
        $data['url']=$list['qianbao_url'];
        $data['name']=$list['name'];
        $data['num']=$num;
        $data['actual']=$actual;//实际到账价格
        $data['status']=0;
        $data['add_time']=time();

        $tibi=$this->qianbao_tibi($list['qianbao_url'],$actual,$currency);//提币程序

        if($tibi){//成功写入数据库
            $data['ti_id']=$tibi;
            $re=M("Tibi")->add($data);
            //减钱操作
            M("Currency_user")->where("member_id='{$_COOKIE['USER_KEY_ID']}' and currency_id='$cuid'")->setDec("num",$num);
            $arr['status']=1;
            $arr['info']="提币成功，请耐心等待";
            $this->ajaxReturn($arr);exit;

        }else{//失败提示
            $arr['status']=9;
            $arr['info']="提币失败";
            $this->ajaxReturn($arr);exit;
        }

    }

    /**
     * 添加钱包提现地址
     */
    public function add_qianbao_address(){
        $cuid=I("currency_id");//货币id
        $currency=M("Currency")->where("currency_id = '$cuid'")->find();
        if(empty($currency)){
            $arr['status']=2;
            $arr['info']="无效币种，无法添加提币地址";
            $this->ajaxReturn($arr);
        }
        if(empty($_POST['name'])){
            $arr['status']=3;
            $arr['info']="新地址姓名不能为空";
            $this->ajaxReturn($arr);
        }
        if(empty($_POST['address'])){
            $arr['status']=4;
            $arr['info']="新地址不能为空";
            $this->ajaxReturn($arr);
        }
        //检测地址是否已经存在
        $where['qianbao_url']=$_POST['address'];
        $where['user_id'] = cookie('USER_KEY_ID');
        $re=M("Qianbao_address")->where($where)->find();
        if(!empty($re)){
            $arr['status']=6;
            $arr['info']="此地址已经绑定，请核实真实地址";
            $this->ajaxReturn($arr);
        }
        //检查此人是否已经有地址
        // $uq=M("Qianbao_address")->where("user_id='{$_COOKIE['USER_KEY_ID']}'")->find();

        $data['currency_id']=$cuid;//货币id
        $data['name']=$_POST['name'];
        $data['qianbao_url']=$_POST['address'];
        $data['add_time']=time();
        $data['user_id']=$_COOKIE['USER_KEY_ID'];
        $data['status']=1;
        $qa=M("Qianbao_address")->add($data);
        if($qa){
            $arr['status']=1;
            $arr['info']="添加成功";
            $this->ajaxReturn($arr);
        }else{
            $arr['status']=7;
            $arr['info']="添加失败";
            $this->ajaxReturn($arr);
        }
    }

    /**
     * 删除钱包地址
     */
    public function del_address(){
        $id=I('id');
        $cuid=I("cuid");
        if(empty($id)){
            $this->error("无效数据");
        }
        if(empty($cuid)){
            $this->error("无效货币");
        }
        $where['id']=$id;//提币地址的id
        $where['currency_id']=$cuid;//提币的币种id
        $where['user_id']=$_COOKIE['USER_KEY_ID'];
        $qa=M("Qianbao_address")->where($where)->find();
        if(empty($qa)){
            $this->error("非法操作");
        }

        $re=M("Qianbao_address")->where($where)->delete();
        if($re){
            $this->success("删除成功",U('Pay/tcoin',array('currency_id'=>$cuid)));
        }else{
            $this->error("删除失败");
        }
    }
    /**
     * 充值方法
     * @return boolean
     */

    public function chongzhi_function(){
        //      $where['status']=array("in",array(3));//1与3分别为 提币成功 与充值成功;
        //      $where['user_id']=$_COOKIE['USER_KEY_ID'];
        //      $count = M("Tibi")->where($where)->count();
        $id=I("currency_id");//货币id；
        if(empty($id)){
            return false;
        }
        $currency=M("Currency")->where("currency_id='$id'")->find();//这个是货币
        if(empty($currency)){
            return false;
        }

        //如果货币不存在 直接返回
        $currency=M("Currency")->where("currency_id='$id'")->find();
        if(empty($currency)){
            return false;
        }
        $list=$this->trade_qianbao($_COOKIE['USER_KEY'],$currency);
        foreach ($list as $k=>$v){
            $data["currency_id"]=$currency['currency_id'];//货币id写入
            if($v['category']=='receive'){
                $data[]=array();
                $data['user_id']=$_COOKIE['USER_KEY_ID'];
                $data['url']=$v['address'];//地址
                $data['name']=$v['account'];//标签
                $data['add_time']=$v['time'];//时间
                $data['num']=$v['amount'];//数量
                $tibi_txid=M("Tibi")->where("ti_id='{$v['txid']}'")->find();
                if(!empty($tibi_txid)){
                    //如果已经存在  而且是已经完成状态 不处理直接跳出循环
                    if($tibi_txid['status']==3){
                        continue;
                    }
                    if($v['confirmations']>2){
                        $data['status']=3;//3表示充值完成
                        $data['check_time']=$v['timereceived'];//确认时间
                        $re=M("Tibi")->where("ti_id='{$v['txid']}'")->save($data);//修改状态 表示已经完成
                        M("Currency_user")->where("member_id='{$_COOKIE['USER_KEY_ID']}' and currency_id='$id'")->setInc("num",$v['amount']);//给User表加钱
                    }
                }else{
                    $data['ti_id']=$v['txid'];//写入交易id号
                    if($v['confirmations']>2){
                        $data['status']=3;//3表示充值完成
                        $data['check_time']=$v['timereceived'];//确认时间
                        $re=M("Tibi")->add($data);//修改状态 表示已经完成
                        M("Currency_user")->where("member_id='{$_COOKIE['USER_KEY_ID']}'  and currency_id='$id' ")->setInc("num",$v['amount']);//给User表加钱
                    }else{
                        $data['status']=2;//2表示充值中
                        $re=M("Tibi")->add($data);
                    }
                }
            }
        }
        if($re){
            $arr['status']=1;
            $this->ajaxReturn($arr);
        }


    }


    /**
     * 提币引用的方法
     * @param unknown $url 钱包地址
     * @param unknown $money 提币数量
     *
     * 需要加密 *********************
     */
    private function qianbao_tibi($url,$money,$currency){
        require_once 'App/Common/Common/easybitcoin.php';
        $bitcoin = new \Bitcoin($currency['rpc_user'],$currency['rpc_pwd'],$currency['rpc_url'],$currency['port_number']);
        //$result = $bitcoin->getinfo();
        $bitcoin->walletlock();//强制上锁
        $bitcoin->walletpassphrase($currency['qianbao_key'],20);
        $id=$bitcoin->sendtoaddress($url,$money);
        $bitcoin->walletlock();
        return $id;
    }


    /**
     * 查询某人的交易记录
     * @param unknown $user 用户名
     * @param unknown $count  从第几个开始查找
     * @return $list  返回此用户的交易列表
     */
    private function trade_qianbao($user,$currency){
        require_once 'App/Common/Common/easybitcoin.php';
        $bitcoin = new \Bitcoin($currency['rpc_user'],$currency['rpc_pwd'],$currency['rpc_url'],$currency['port_number']);
        $result = $bitcoin->getinfo();
        $list=$bitcoin->listtransactions($user,10,0);
        return $list;
    }



    public function rpc2(){
        require_once 'App/Common/Common/easybitcoin.php';
        //require_once APP_PATH.'Common/Common/Common/easybitcoin.php';
        $bitcoin = new \Bitcoin('green','123456','47.89.49.145','29992');
        echo $bitcoin->error;
        dump($bitcoin);
        $result = $bitcoin->getinfo();
        echo $result;
    //  $id= $bitcoin->sendtoaddress('LXUVqocGoVivuEXd4SPquZC3W5eW7DVCMD',0.00001);

    }

    /**
     * 获取新的一个钱包地址
     * @return unknown
     */
    private function qianbao_new_address($currency){
        require_once 'App/Common/Common/easybitcoin.php';
        $bitcoin = new \Bitcoin($currency['rpc_user'],$currency['rpc_pwd'],$currency['rpc_url'],$currency['port_number']);
        $user=$_COOKIE['USER_KEY'];
        //dump($bitcoin);
        $address = $bitcoin->getnewaddress($user);
        //dump($address);
        return $address;
    }
    /**
     * 检测地址是否是有效地址
     *
     * @return boolean 如果成功返回个true
     * @return boolean 如果失败返回个false；
     *  @param unknown $url
     *  @param $port_number 端口号 来区分不同的钱包
     */
    private function check_qianbao_address($url,$currency){

        require_once 'App/Common/Common/easybitcoin.php';
        $bitcoin = new \Bitcoin($currency['rpc_user'],$currency['rpc_pwd'],$currency['rpc_url'],$currency['port_number']);
        $address = $bitcoin->validateaddress($url);
        if($address['isvalid']){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 功能：生成二维码
     * @param string $qr_data   手机扫描后要跳转的网址
     * @param string $qr_level  默认纠错比例 分为L、M、Q、H四个等级，H代表最高纠错能力
     * @param string $qr_size   二维码图大小，1－10可选，数字越大图片尺寸越大
     * @param string $save_path 图片存储路径
     * @param string $save_prefix 图片名称前缀
     */
     function createQRcode($save_path,$qr_data='PHP QR Code :)',$qr_level='L',$qr_size=4,$save_prefix='qrcode'){
        if(!isset($save_path)) return '';
        //设置生成png图片的路径
        $PNG_TEMP_DIR = & $save_path;
        //导入二维码核心程序
        vendor('class#phpqrcode');  //注意这里的大小写哦，不然会出现找不到类，PHPQRcode是文件夹名字，class#phpqrcode就代表class.phpqrcode.php文件名
        //检测并创建生成文件夹
        if (!file_exists($PNG_TEMP_DIR)){
            mkdir($PNG_TEMP_DIR);
        }
        $filename = $PNG_TEMP_DIR.'test.png';
        $errorCorrectionLevel = 'L';
        if (isset($qr_level) && in_array($qr_level, array('L','M','Q','H'))){
            $errorCorrectionLevel = & $qr_level;
        }
        $matrixPointSize = 4;
        if (isset($qr_size)){
            $matrixPointSize = & min(max((int)$qr_size, 1), 10);
        }
        if (isset($qr_data)) {
            if (trim($qr_data) == ''){
                die('data cannot be empty!');
            }
            //生成文件名 文件路径+图片名字前缀+md5(名称)+.png
            $filename = $PNG_TEMP_DIR.$save_prefix.md5($qr_data.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
            //开始生成
            \QRcode::png($qr_data, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
        } else {
            //默认生成
            \QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);
        }
        if(file_exists($PNG_TEMP_DIR.basename($filename)))
            return basename($filename);
        else
            return FALSE;
     }

}
