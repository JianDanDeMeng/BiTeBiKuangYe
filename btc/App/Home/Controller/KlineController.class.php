<?php
namespace Home\Controller;
use Common\Controller\CommonController;
class KlineController extends CommonController {
    public function trade(){
        $db = M('Kline');
        $where['cid'] = $_POST['cid'];
        $where['type'] = $_POST['type'];
        $data = $db->where($where)->select();
        for($i=0;$i<count($data);$i++){
            $datas[$i][] = (float)($data[$i]['add_time']."000");
            $datas[$i][] = (float)$data[$i]['start'];
            $datas[$i][] = (float)$data[$i]['max'];
            $datas[$i][] = (float)$data[$i]['min'];
            $datas[$i][] = (float)$data[$i]['end'];
            $datas[$i][] = (float)$data[$i]['num'];
        }
        
        $result['des'] = "";
        $result['isSuc'] = true;
        $result['datas']['USDCNY'] = 6.5746;
        $result['datas']['contractUnit'] = "BTC";
        $result['datas']['data'] = $datas;
        $result['marketName'] = "YUANBAO";
        $result['moneyType'] = "cny";
        $result['symbol'] = "btc2cny";
        $result['url'] = "https://www.yuanbaohui.com";

        $this->ajaxReturn($result);
    }

}