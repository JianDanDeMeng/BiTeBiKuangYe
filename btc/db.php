<?php
class db{
	private $mysqli;		//定义数据库对像
	private $options;		//
	private $tabName;		//表名
	private $sql;			//执行的sql语句
	
	public function __construct($tableName){
		$this->tabName = $tableName;
		$this->options['field'] = '';
		$this->options['where'] = '';
		$this->options['order'] = '';
		$this->db();
	}
	
	private function db(){
		$this->mysqli = new mysqli('172.19.0.6','btckycc','pr3y5LZID50q','btckycc');
		
		$this->mysqli->set_charset('utf8');
	}

	public function field($str){
		$this->options['field'] = $str;
		return $this;
	}

	public function where($str){
		$this->options['where'] = " WHERE ".$str;
		return $this;
	}

	public function order($str){
		$this->options['order'] = " order by ".$str;
		return $this;
	}

	public function select(){
		if(empty($this->options['field'])){
			$this->sql = "SELECT * FROM {$this->tabName} {$this->options['where']}  {$this->options['order']}";
		}else{
			$this->sql = "SELECT {$this->options['field']} FROM {$this->tabName} {$this->options['where']} {$this->options['order']}";
		}
		return $this->query();
	}

	public function checkFields($ety){
		$sql = "SHOW COLUMNS FROM ".$this->tabName;
		$result = $this->mysqli->query($sql);
		while($row = $result->fetch_assoc()){
			$rows[] = $row['Field'];
		}
		foreach($ety as $key=>$value){
			if(!in_array($key,$rows)){
				unset($ety[$key]);
			}
		}
		return $ety;
	}

	public function insert($ety){
		$ety = $this->checkFields($ety);
		$key = implode(",",array_keys($ety));
		$value = implode("','",$ety);
		$this->sql = "INSERT INTO {$this->tabName}({$key}) VALUES('{$value}')";
		//echo $this->sql;
		return $this->query();
	}

	public function save($ety,$key="id"){
		$ety = $this->checkFields($ety);
		$sql = '';
		foreach($ety as $k=>$v){
			$sql .= ",{$k}='$v'";
		}
		$sql = substr($sql,1);
		$this->sql = "UPDATE {$this->tabName} SET {$sql} WHERE {$key}='{$ety[$key]}'";
		//echo $this->sql;
		return $this->query();
	}

	public function deleteById($id,$key="id"){
		$this->sql = "DELETE FROM {$this->tabName} WHERE {$key}={$id}";
		return $this->query();
	}

	public function query($sql=""){
		if(strlen($sql)>0){
			$this->sql = $sql;
		}
		$result = $this->mysqli->query($this->sql);
		if(is_object($result)){
			$rows = array();
			while($row = $result->fetch_assoc()){
				$rows[] = $row;
			}
			//echo $this->sql;
			return $rows;
		}else{
			return $result;
		}
	}
	
	public function sq(){
		echo $this->sql;
	}

	private function close(){
		$this->mysqli->close();
	}

	public function __destruct(){
		$this->close();
	}
}
function dump($var){
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}
?>